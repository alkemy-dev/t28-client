import React from "react";
import HomeIcon from '@material-ui/icons/Home';
import AddBoxIcon from "@material-ui/icons/AddBox";
import EditIcon from "@material-ui/icons/Edit";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

// Routes
export const HOME_ROUTE = "/";
export const ABOUT_US_ROUTE = "/nosotros";
export const USER_ROUTE = "/user";
export const NEWS_ROUTE = "/novedades";
export const ACTIVITIES_ROUTE = "/actividades";
export const CONTACTS_ROUTE = "/contactos";
export const TESTOMINIES_ROUTE = "/testimonios";
export const LOGIN_ROUTE = "/login";
export const REGISTER_ROUTE = "/registro";

//Api
export const API_URL = "http://localhost:3000";

//Tables
export const AUTH_TABLE = "auth";
export const AUTH_LOGIN = "login";
export const AUTH_REGISTER = "register";
export const USERS_TABLE = "users";
export const NEWS_TABLE = "news";
export const CONTACTS_TABLE = "contacts";
export const USERS_CONTACTS = "contacts";
export const CATEGORIES_TABLE = "categories";
export const ACTIVITIES_TABLE = "activities";
export const TESTIMONIES_TABLE = "testimonials";

//Alert Types
export const ALERT_CONFIRM = "alert-confirm";
export const ALERT_ERROR = "alert-error";
export const ALERT_INFO = "alert-info";

export const DashBoardsLinks = [
  {
    title: 'Inicio',
    icons: <HomeIcon />,
    link: '/'
  },
  {
    title: 'Mi Perfil',
    icons: <EditIcon />,
    link: '/backoffice/profile'
  },
  {
    title: 'Cerrar Session',
    icons: <ExitToAppIcon />,
    link: '/logout'
  },
  /*
  {
    title: "Crear item",
    icons: <AddBoxIcon />,
    link: "/backoffice",
  },
  {
    title: "Modificar item",
    icons: <EditIcon />,
    link: "/backoffice",
  },
  {
    title: "Borrar item",
    icons: <DeleteForeverIcon />,
    link: "/backoffice",
  },
  {
    title: "Estado item",
    icons: <AccessTimeIcon />,
    link: "/backoffice",
  },
  */
];

//Request Methods
export const REQUEST_METHOD_GET = "GET";
export const REQUEST_METHOD_POST = "POST";
export const REQUEST_METHOD_PUT = 'PUT';
export const REQUEST_METHOD_PATCH = "PATCH";
export const REQUEST_METHOD_DELETE = "DELETE";

//Errors & Messages
export const EMPTY_VALUE_MESSAGE = "Complete el campo";
export const MINIMUM_VALUE_MESSAGE = "Debe tener 20 caracteres como minimo";

// -- Entries Form  -- //
export const CREATE_ENTRY_ERROR_TITLE = "Error creando entrada";
export const CREATE_ENTRY_ERROR_TEXT = "Hubo un error creando la entrada";
export const CREATE_ENTRY_ERROR_CONFIRM_TEXT = "Cerrar";
export const EMPTY_ENTRY_ERROR_TITLE = "Error creando entrada";
export const EMPTY_ENTRY_ERROR_TEXT = "El contenido no puede estar vacio";
export const EMPTY_ENTRY_ERROR_CONFIRM_TEXT = "Aceptar";

// Testimony
export const TESTIMONY_DELETE_QUESTION = "¿Desea eliminar el Testimonio?";
export const TESTIMONY_EDIT = "Testimonio Editado";

// -- Testimony Form  -- //
export const CREATE_TESTIMONY_ERROR_TITLE = "Error";
export const CREATE_TESTIMONY_ERROR_TEXT = "Hubo un error creando la entrada";
export const CREATE_TESTIMONY_ERROR_CONFIRM_TEXT = "Cerrar";
export const EMPTY_TESTIMONY_ERROR_TITLE = "Error";
export const EMPTY_TESTIMONY_ERROR_TEXT = "El contenido no puede estar vacio";
export const EMPTY_TESTIMONY_ERROR_CONFIRM_TEXT = "Aceptar";
export const EDIT_TESTIMONY_ERROR_TITLE = "Error";
export const EDIT_TESTIMONY_ERROR_TEXT = "Hubo un error editando el testimonio";
export const EDIT_TESTIMONY_ERROR_CONFIRM_TEXT = "Cerrar";

//Initial Values
export const INITIAL_VALUES_HOME_FORM = {
  wellcome: "",
  title1: "",
  image1: null,
  title2: "",
  image2: null,
  title3: "",
  image3: null,
};

// Form General Values
export const ACCEPT = 'Aceptar';
export const CANCEL = 'Cancelar';
export const ERROR = 'Error';
export const SAVE = 'Guardar';
export const UPDATE = 'Update';

//Form Auth errors
export const EMAIL_INVALID = "El email no es válido";
export const FIELD_REQUIRED = "Este campo es obligatorio";
export const PASSWORD_ERROR = "Los Passwords no coinciden";

// Redux status
export const IDLE = "idle";
export const LOADING = "loading";
export const SUCCEEDED = "succeeded";
export const FAILED = "failed";
export const ERROR_NULL = null;

// Form message
export const errorMaxCharacters = (character) => {
  return `Este campo debe tener maximo ${character} caracteres`;
};

export const errorMinCharacters = (character) => {
  return `Este campo debe tener mínimo ${character} caracteres`;
};

//Categories messages
export const errorDeleteCategory = (category) => {
  return `Error al eliminar la categoria ${category}`;
};

export const successDeleteCategory = (category) => {
  return `La categoria ${category} se elimino correctamente`;
};

//News messages
export const errorDeleteNews = (news) => {
  return `Error al eliminar la novedad ${news}`;
};
export const successDeleteNews = (news) => {
  return `La novedad ${news} se elimino correctamente`;
};

// Users List Table messages
export const successDeleteUser = (email) => {
  return `El Usuario ${email} se eliminó correctamente`;
}

export const errorDeleteUser = (email) => {
  return `Error al eliminar el Usuario ${email}`;
}

// Testimonies List Table messages
export const successDeleteTestimony = (testimony) => {
  return `El Testimonio ${testimony} se eliminó correctamente`;
}

export const errorDeleteTestimony = (testimony) => {
  return `Error al eliminar el Testimonio ${testimony}`;
}
import React, { Fragment } from 'react';
import { Link } from "react-router-dom";
import { 
    Box, 
    Image, 
    Badge, 
    Button, 
    Text
} 
from '@chakra-ui/react';

const property = {    
    imageAlt: "Rear view of modern home with pool",
    beds: 3,
    baths: 2,
    title: "Noticia numero uno de nuestra ONG",
    formattedPrice: "$1,900.00",
    reviewCount: 34,
    rating: 4    
}

const ActivityCard = ({ data }) => {    
      console.log(data)  
    return(        
        <Fragment>
            <Box m={5} maxW="340px" borderWidth="1px" borderRadius="lg" overflow="hidden" className="bg-gray-50">
                <Image h="250px" src={data.image} alt={property.imageAlt} />        
                <Box p="6">
                    <Box d="flex" justifyContent="space-between" mb={2}>
                            
                        <Box            
                        
                        fontWeight="semibold"
                        as="h4"
                        lineHeight="tight"
                                    
                    >
                        {data.name}
                    </Box>   
                    <Badge borderRadius="full" px="1" colorScheme="red">
                            {data.createdAt.substring(0,10)}
                        </Badge>             
                    </Box>            
                          
                    
                        <Text color="gray.500" isTruncated p={0}>
                            {data.content}
                        </Text>
                      
                                 
                </Box>
            </Box>
        </Fragment>
    )
}

export default ActivityCard;
import React, { useEffect, useState, Fragment } from "react";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { selectActivities, activitiesStatus, getActivities } from '../../features/activitySlice/activitySlice';
import { IDLE } from '../../constants/index';
import Loader from '../../components/loader/LoaderComponent';
//Functions
import { getActivityDetails } from "./functions";

import "./index.sass";
import ActivityCard from "./ActivityCard";
import { Center, Flex, Text } from "@chakra-ui/layout";

const ActivityScreen = () => {
  const data = useSelector(selectActivities);
  const activitiesStatus = useSelector(state => state.activities.status);
  const dispatch = useDispatch();

  console.log("Data: ", {data})
  // const { id } = useParams();
  // const [details, setDetails] = useState({
  //   name: "",
  //   content: "",
  // });

  // //Appends content to DOM
  // useEffect(() => {
  //   document.querySelector("#content").innerHTML = details.content;
  // }, [details]);

  // //Get Activity Details
  // useEffect(async () => {
  //   getActivityDetails(id, setDetails);
  // }, []);

  useEffect( () => {
    if(activitiesStatus === IDLE) {
      dispatch(getActivities());
    }
  }, [activitiesStatus]);


  return (
    <Fragment>
    <div className="background-yellow py-1">
        <Center>
            <Text fontSize="4xl" my='2'>Actividades</Text>
        </Center>            
        <Flex justify="center" flexWrap="wrap">       
            {data.map((item) => {
                return(
                    <ActivityCard key={item.id} data={item}/>   
                )                    
            })}                
        </Flex>
    </div> 
</Fragment>
  );
};

export default ActivityScreen;

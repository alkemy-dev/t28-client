import { ApiService } from "../../GeneralServices";

//Alert Related
import { alertError } from "../../components/Alert";

//Constants
import { REQUEST_METHOD_GET, API_URL } from "../../constants";

export const getActivityDetails = (id, setDetails) => {
  const error = {
    title: "Error",
    text: "No se ha encontrado la actividad",
    confirmText: "Cerrar",
  };

  const fakeData = {
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed",
    content:
      "<h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed </h2><h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed </h3>",
  };

  ApiService({
    API_URL: API_URL,
    METHOD: REQUEST_METHOD_GET,
    BASE: "actividades",
    ID: id,
  })
    .then((res) => {
      if (res) {
        setDetails(res);
      } else {
        setDetails(fakeData);
      }
    })
    .catch((err) => {
      alertError(error);
      console.log(err);
    });
};

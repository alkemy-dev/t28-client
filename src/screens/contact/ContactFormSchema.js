import * as Yup from "yup";

const ContactFormSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "Ingrese por lo menos 2 caracteres")
    .max(50, "El nombre ingresado es demasiado largo")
    .required("*Obligatorio"),
  phone: Yup.number()
    .required("*Obligatorio"),
  email: Yup.string()
    .email("Correo electronico inválido")
    .required("*Obligatorio"),
  message: Yup.string()
    .required("*Obligatorio")
    .min(10, "Ingrese por lo menos 10 caracteres"),
});

export default ContactFormSchema;

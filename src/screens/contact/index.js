import React from "react";
import { Formik, Field, Form } from "formik";

//Chakra Imports
import { Button, Input, Textarea } from "@chakra-ui/react";
import { FormControl, FormErrorMessage } from "@chakra-ui/react";

//Styles
import "./Contact.sass";

//Validation Schema
import ContactFormSchema from "./ContactFormSchema";


import { useDispatch } from 'react-redux';
import { createContact } from '../../features/contact/contactSlice';

const ContactScreen = () => {

  const dispatch = useDispatch()

  const handleSubmit = (values, onSubmitProps) => {
    dispatch(createContact(values)); 
    onSubmitProps.resetForm()
  };
  

  return (
    <div id="contact" className="screen">
      <div className="card">
        <h2 className="title">Contactate con nosotros</h2>
        <Formik
          initialValues={{
            name: "",
            phone: "",
            email: "",
            message: "",
          }}
          onSubmit={(values, onSubmitProps)=>handleSubmit(values, onSubmitProps)}
          validationSchema={ContactFormSchema}        
        >
          {(props) => (
            <Form className="form">
              {/* FirstName & LastName Wrapper */}
              <div className="form-group">
                {/* First Name */}
                <Field name="name">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={
                        form.errors.name && form.touched.name
                      }
                    >
                      <FormErrorMessage className="input-error">
                        {form.errors.name}
                      </FormErrorMessage>

                      <Input {...field} id="name" placeholder="Nombre" />
                    </FormControl>
                  )}
                </Field>

                {/* LastName */}
                <Field name="phone">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={form.errors.phone && form.touched.phone}
                    >
                      <FormErrorMessage className="input-error">
                        {form.errors.phone}
                      </FormErrorMessage>
                      <Input {...field} id="phone" placeholder="Teléfono" />
                    </FormControl>
                  )}
                </Field>
              </div>

              {/* Email */}
              <Field name="email">
                {({ field, form }) => (
                  <FormControl
                    isInvalid={form.errors.email && form.touched.email}
                  >
                    <FormErrorMessage className="input-error">
                      {form.errors.email}
                    </FormErrorMessage>

                    <Input {...field} id="email" placeholder="Email" />
                  </FormControl>
                )}
              </Field>

              {/* Message */}
              <Field name="message">
                {({ field, form }) => (
                  <FormControl
                    isInvalid={form.errors.message && form.touched.message}
                  >
                    <FormErrorMessage className="input-error">
                      {form.errors.message}
                    </FormErrorMessage>

                    <Textarea
                      {...field}
                      id="message"
                      placeholder="Escribe tu consulta..."
                    />
                  </FormControl>
                )}
              </Field>

              {/* Submit Button */}
              <Button
                mt={4}
                colorScheme="teal"                
                type="submit"
                size="lg"
                padding="8"
                
              >
                Enviar
              </Button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default ContactScreen;

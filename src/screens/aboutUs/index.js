import React from 'react';
import "./aboutUs.css";
import MemberCard from '../../components/MemberCard/MemberCard';
import ongMembers from '../../components/MemberCard/fakeMembers';

const AboutUs = () => {
    return (
<div>
        <div className="contentAboutUs">
            <div className="section">
                <div className="title">Sobre la ONG</div>
            
                <div className="subtitle">Nosotros</div>
                <p>Desde 1997 en Somos Más trabajamos con los chicos y chicas, mamás y papás, abuelos y vecinos del barrio La Cava generando procesos de crecimiento y de inserción social. Uniendo las manos de todas las familias, las que viven en el barrio y las que viven fuera de él, es que podemos pensar, crear y garantizar estos procesos. ﻿ Somos una asociación civil sin fines de lucro que se creó en 1997 con la intención de dar alimento a las familias del barrio. Con el tiempo fuimos involucrándonos con la comunidad y agrandando y mejorando nuestra capacidad de trabajo. Hoy somos un centro comunitario que acompaña a más de 700 personas a través de las áreas de: Educación, deportes, primera infancia, salud, alimentación y trabajo social. </p>
            
                <div className="subtitle">Visión</div>
                <p>Mejorar la calidad de vida de niños y familias en situación de vulnerabilidad en el barrio La Cava, otorgando un cambio de rumbo en cada individuo a través de la educación, salud, trabajo, deporte, responsabilidad y compromiso.</p>

                <div className="subtitle">Misión</div>
                <p>Trabajar articuladamente con los distintos aspectos de la vida de las familias, generando espacios de desarrollo personal y familiar, brindando herramientas que logren mejorar la calidad de vida a través de su propio esfuerzo.</p>
            </div>
        </div>
        
        <div style={{display:'flex', 'flex-wrap': 'wrap', 'justifyContent':'center'}}>
            { ongMembers.map( item => 
                <MemberCard data={item} />
            )}
        </div>
        
</div>
    );
}
 
export default AboutUs;
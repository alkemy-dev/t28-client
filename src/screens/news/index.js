import { Center, Flex, Text } from "@chakra-ui/layout";
import React, { useEffect } from "react";
import NewsCard from "../../components/Home/NewsCard";

import { IDLE } from "../../constants/index";

import { useDispatch, useSelector } from "react-redux";
import { getNews, selectNews } from "../../features/news/newsSlice";

const NewsScreen = () => {
  const data = useSelector(selectNews);
  const dispatch = useDispatch();
  console.log(data[0]);
  const newsStatus = useSelector((state) => state.news.status);

  useEffect(() => {
    if (newsStatus === IDLE && data.length === 0) {
      dispatch(getNews());
    }
  }, [newsStatus]);

  return (
    <div className="background-yellow">
      <Center>
        <h1>
          <Text fontSize={50} m="5" p="5">
            Novedades
          </Text>
        </h1>
      </Center>
      <Center>
        <Text fontSize={25} textAlign="center">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae
          voluptas, sequi vel ab dolorum illo perferendis, aliquid repudiandae
          optio reprehenderit quod.
        </Text>
      </Center>
      <Center>
        <Flex justify="center" flexWrap="wrap">
          {data.map((item) => {
            return <NewsCard key={item.id} data={item} />;
          })}
        </Flex>
      </Center>
    </div>
  );
};

export default NewsScreen;

import React, { useEffect, useState } from 'react';
import { Button } from '@chakra-ui/react'
// icons
import { FaEdit, FaPlus, FaTrash } from 'react-icons/fa';
// material table config
import MaterialTable from '@material-table/core';
import { options, localization, title } from './settings';
// redux store
import { useDispatch, useSelector } from 'react-redux';
import { deleteTestimony, getTestimonies, selectTestimonies } from '../../features/testimony/testimonySlice';
import { cloneDeep } from 'lodash';

import moment from 'moment';

// components
import TestimonyForm from '../../components/TestimonyForm';
import LoaderComponent from '../../components/loader/LoaderComponent';

import { IDLE, SUCCEEDED } from '../../constants';

const TableTestimony = () => {      
  const dispatch = useDispatch();  
    const [modal, setModal] = useState("")
    

    const columns = [
      { 
          title: 'Imagen', 
          field: 'image',
          render: rowData => <img src={rowData.image} alt={`${rowData.name}`} style={{width: 60, borderRadius: '60%'}}/>,
          cellStyle: { width: '10%' } 
      },
      { 
          title: 'Nombre', 
          field: 'name',
          cellStyle: { textAlign: 'center', width: '40%' }
      },    
      { 
          title: 'Fecha de creación', 
          field: 'createdAt',                        
          render: rowData => moment.utc(rowData.date).format('DD/MM/YYYY')
      },
      {                
          cellStyle: { width: '3%' },
          render: rowData => 
          <Button mr={0} colorScheme="yellow" onClick={()=>handleEditeTestimony(rowData)}>
                  <FaEdit />
              </Button>
      },
      {
          cellStyle: { width: '3%' },
          render: rowData => 
          <Button colorScheme="red" onClick={()=>handleDeleteTestimony({name:rowData.name, id:rowData.id})}>
          <FaTrash />
      </Button>            
      }
  ];
 
    const handleDeleteTestimony = (dataTestimony)=>{
        dispatch(deleteTestimony(dataTestimony))
        dispatch(getTestimonies())       
    }


    const handleEditeTestimony = (dataTestimony) =>{       
        setModal(<TestimonyForm testimony={dataTestimony} setModal={setModal}/>)
        dispatch(getTestimonies());           
    }    
    
    const testimonies = useSelector(selectTestimonies);    
    
    const flexData = cloneDeep(testimonies);

    const testimonialStatus = useSelector(state => state.testimony.status);
    
    useEffect(() => {
      if(testimonialStatus === IDLE)
          dispatch(getTestimonies());                      
               
    }, [modal, dispatch])

    console.log(testimonies)

    return (
      <div className="md:w-11/12 xl:w-100 mx-auto mb-5">
        { testimonialStatus !== SUCCEEDED ? 
        <div className='flex justify-center items-center' style={{'min-height': '60vh'}}>
            <LoaderComponent />
        </div>
        :
        <>
        <MaterialTable
          columns={columns}
          data={flexData}
          title={title}
          options={options}
          localization={localization}
          actions={[
            {
              icon: () => <div style={{fontSize: '1em', color: 'rgb(40,110,251)'}}><FaPlus /></div>,
              tooltip: "Crear Usuario",
              position: "toolbar",
              onClick: () => {
                setModal(<TestimonyForm setModal={setModal} />);
              }
            }
          ]}
        />
        {modal}
        </>}
      </div>
    );
}
export default TableTestimony;
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { ApiService } from '../../GeneralServices/index';
import { API_URL, NEWS_TABLE, REQUEST_METHOD_GET, IDLE, LOADING, SUCCEEDED, FAILED, REQUEST_METHOD_DELETE, errorDeleteNews, successDeleteNews } from '../../constants/index';
import { alertError, alertSuccess } from '../../components/Alert';

export const getNews = createAsyncThunk(
	'news/getNews',
	async () => {
		return await ApiService({
			API_URL: API_URL,
			BASE: NEWS_TABLE,
			METHOD: REQUEST_METHOD_GET,
		});
	}
);

export const deleteNews = createAsyncThunk(    
    'news/deleteNews',
    async (data)=>{
    return await ApiService({
        API_URL: API_URL,
        BASE: 'news/'+data.id,               
        METHOD: REQUEST_METHOD_DELETE       
    })
    .then(res => {
        if(res === undefined) throw new Error(errorDeleteNews(data.id));
        const successAlert = {
            title: "Listo!",
            text: successDeleteNews(data.name),
            confirmText: 'Aceptar'
        }
          alertSuccess(successAlert)                   
                      
    })
    .catch(error => {
        console.log(error)
        const errorAlert = {
            title: 'Error',
            text: error.message,
            confirmText: 'Aceptar'
        }
          alertError(errorAlert)
    })
})


const newsSlice = createSlice({
	name: 'news',
	initialState: {
		data: [],
		status: IDLE
	},
	extraReducers: {
		[getNews.pending]: (state, action) => {
			state.status = LOADING
		},
		[getNews.fulfilled]: (state, action) => {
			state.status = SUCCEEDED;
			state.data = action.payload.entries;
		},
		[getNews.rejected]: (state, action) => {
			state.status = FAILED;
		}
	}
}) 

// Selectors
export const selectNews = state => state.news.data;

export const selectLatestNews = state => state.news.data.slice(0,4);

export default newsSlice.reducer;

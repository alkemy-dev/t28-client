import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { ApiService } from '../../GeneralServices/index';
import { API_URL, CONTACTS_TABLE, REQUEST_METHOD_POST } from '../../constants/index';
import { alertError, alertSuccess } from '../../components/Alert';

export const createContact = createAsyncThunk(
    'contacts/createContact',
    async (payload) => {
        await ApiService({
            API_URL: API_URL,
            BASE: CONTACTS_TABLE,
            METHOD: REQUEST_METHOD_POST,
            data: payload
        }).then(response => {
            const content = response.insertContact
            
            const successAlert = {
                title: "Exito!",
                text: `Hola ${content.name} tu consulta se envio correctamente`,
                confirmText: "Aceptar",
              };
              alertSuccess(successAlert);            
        })
        .catch(error => {
            const errorAlert = {
                title: "Error",
                text: error.message,
                confirmText: "Aceptar",
              };
              alertError(errorAlert);
            console.log(error)
        })
    }
)

export const contactSlice = createSlice({
    name: "contact",
    initialState:{
        contacts: [],
    },
    extraReducers: {
        [createContact.fulfilled]: (state, action) => {
            state.contacts.push(action.payload)
        }
    }
});

export const contactSelector = (state) => state.contact;

export default contactSlice.reducer;
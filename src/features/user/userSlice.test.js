import {loginSuccess, logout} from './userSlice';


describe('Login Success', ()=>{
    test('Login', () =>{
               
        const action = {
                userName: "user",
                email: "user@user.com",
                token: "TOKEN"
        }
        
        const newState = loginSuccess(action);
       
        expect(newState).toEqual({
            type: 'user/loginSuccess',
            payload: { userName: 'user', email: 'user@user.com', token: 'TOKEN' }
        });       
        
    })    
})

describe('Logout', ()=>{
    test('Logout', () =>{
        
        const action = {}

        const newState = logout(action);
        
        expect(newState).toEqual({
            type: 'user/logout',
            payload: {}
          });
    })
    
})


  
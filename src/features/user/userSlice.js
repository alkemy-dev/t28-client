import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { ApiService } from "../../GeneralServices";
import {
  API_URL,
  USERS_TABLE,
  FAILED,
  LOADING,
  REQUEST_METHOD_GET,
  REQUEST_METHOD_DELETE,
  SUCCEEDED,
  IDLE,
  errorDeleteUser,
  successDeleteUser,
  REQUEST_METHOD_POST,
} from "../../constants";
import { alertError, alertSuccess } from "../../components/Alert";

const jsonValidation = (name) => {
  try {
    const validation = JSON.parse(localStorage.getItem(name)) !== null;
    console.log("la validacion de los datos es: ", validation);
    if (JSON.parse(localStorage.getItem(name) === "{}")) {
      return false;
    }
    return validation;
  } catch (e) {
    console.log("not json");
    return false;
  }
};

export const getUsers = createAsyncThunk("user/getUsers", async () => {
  return await ApiService({
    API_URL: API_URL,
    BASE: USERS_TABLE,
    METHOD: REQUEST_METHOD_GET,
  });
});

export const deleteUser = createAsyncThunk("user/deleteUser", async (data) => {
  return await ApiService({
    API_URL: API_URL,
    BASE: "users/" + data.id,
    METHOD: REQUEST_METHOD_DELETE,
  })
    .then((res) => {
      if (res === undefined) throw new Error(errorDeleteUser(data.id));
      const successAlert = {
        title: "Listo!",
        text: successDeleteUser(data.email),
        confirmText: "Aceptar",
      };
      alertSuccess(successAlert);
    })
    .catch((error) => {
      console.log(error);
      const errorAlert = {
        title: "Error",
        text: error.message,
        confirmText: "Aceptar",
      };
      alertError(errorAlert);
    });
});

export const loginUser = createAsyncThunk(
  "user/loginUser",
  async ({ user }) => {
    try {
      const res = await fetch(API_URL + "/auth/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(user),
      });
      const data = await res.json();
      console.log("los datos cargados son: ", data);
      return data;
    } catch (error) {
      console.log("huboi un error", error);
    }
  }
);

// User Slice
export const userSlice = createSlice({
  name: "user",
  initialState: {
    user: {
      /*  firstName: "user.name",
      lastName: "user.surname",
      email: "fakemail@ong.com",
      roleId: 1, */
    },
    token: "",
    usersList: [],
    status: IDLE,
    error: null,
  },
  reducers: {
    loginSuccess: (state, action) => {
      console.log("los datos del login son estos:", action.payload);
      if (action.payload.status === 200) {
        state.user = action.payload.user;
        state.token = action.payload.token;
        localStorage.setItem("user", JSON.stringify(state.user));
        localStorage.setItem("token", JSON.stringify(state.token));
      } else {
        console.log("HAY UN ERROR EN EL LOGIN: ", action.payload);
      }
    },
    logout: (state) => {
      state.user = {};
      state.token = "";
      localStorage.removeItem("user");
      localStorage.removeItem("token");
    },
    checkUser: (state) => {
      if (jsonValidation("user")) {
        const localUser = JSON.parse(localStorage.getItem("user"));
        const localToken = JSON.parse(localStorage.getItem("token"));
        if (localUser.firstName && localUser.email) {
          state.user = localUser;
          state.token = localToken;
          console.log("el usuario es", state.user.email);
        }
      } else {
        console.log("no hay usuarios guardados en cookies!");
      }
    },
    addUser: (state, action) => {
      state.usersList = state.usersList.concat(action.payload);
    },
    updateUser: (state, action) => {
      // <- check splice not working properly
      const i = state.usersList.findIndex(
        (user) => user.id === action.payload.id
      );
      state.usersList = state.usersList.splice(i, 1, action.payload);
    },
  },
  extraReducers: (builder) => {
    /*  [getUsers.pending]: (state) => {
      state.status = LOADING;
    },
    [getUsers.fulfilled]: (state, action) => {
      state.usersList = state.usersList.concat(action.payload.users);
      state.status = SUCCEEDED;
    },
    [getUsers.rejected]: (state) => {
      state.status = FAILED;
    },
    [deleteUser.fulfilled]: (state, action) => {
      state.usersList = state.usersList.filter(
        (user) => user.id !== action.meta.arg.id
      );
    },
    [deleteUser.rejected]: (state) => {
      state.error = FAILED;
    }, */
    builder
      .addCase(loginUser.rejected, (state) => {
        state.status = FAILED;
      })
      .addCase(loginUser.pending, (state) => {
        state.status = LOADING;
      })
      .addCase(loginUser.fulfilled, (state, action) => {
        state.status = SUCCEEDED;

        if (action.payload.status === 200) {
          state.user.user = action.payload.user;
          state.token = action.payload.token;
          localStorage.setItem("user", JSON.stringify(state.user));
          localStorage.setItem("token", state.token);
        } else {
          console.log("HAY UN ERROR EN EL LOGIN: ", action.payload);
        }
      });
    builder
      .addCase(deleteUser.rejected, (state) => {
        state.status = FAILED;
      })
      .addCase(deleteUser.pending, (state) => {
        state.status = LOADING;
      })
      .addCase(deleteUser.fulfilled, (state, action) => {
        state.status = FAILED;
        state.usersList = state.usersList.filter(
          (user) => user.id !== action.meta.arg.id
        );
      });
    builder
      .addCase(getUsers.rejected, (state) => {
        state.status = FAILED;
      })
      .addCase(getUsers.pending, (state) => {
        state.status = LOADING;
      })
      .addCase(getUsers.fulfilled, (state, action) => {
        state.status = SUCCEEDED;
        state.usersList = state.usersList.concat(action.payload.users);
      });
  },
});

export const {
  loginSuccess,
  logout,
  checkUser,
  addUser,
  updateUser,
} = userSlice.actions;

export const userSelector = (state) => state.user;
export const tokenSelector = (state) => state.token;

export const usersListSelector = (state) => state.usersList;

export default userSlice.reducer;

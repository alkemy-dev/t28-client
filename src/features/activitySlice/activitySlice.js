import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { ApiService } from '../../GeneralServices/index';
import { API_URL, ACTIVITIES_TABLE, REQUEST_METHOD_GET, IDLE, LOADING, SUCCEEDED, FAILED, REQUEST_METHOD_DELETE } from '../../constants/index';
import { alertError, alertSuccess } from '../../components/Alert';

export const getActivities = createAsyncThunk('activities/getActivities', async () => {
	// Get the activities from our database asyncronously
	return await ApiService({API_URL: API_URL, BASE: ACTIVITIES_TABLE, METHOD: REQUEST_METHOD_GET});
})

export const deleteActivities = createAsyncThunk(
    
    'activities/deleteActivities',
    async (data)=>{
    return await ApiService({
        API_URL: API_URL,
        BASE: 'activities/'+data.id,               
        METHOD: REQUEST_METHOD_DELETE       
    })
    .then(res => {        
        const successAlert = {
            title: "Listo!",
            text: "Se elimino la actividad "+(data.name),
            confirmText: 'Aceptar'
        }
          alertSuccess(successAlert)                   
                      
    })
    .catch(error => {
        console.log(error)
        const errorAlert = {
            title: 'Error',
            text: error.message,
            confirmText: 'Aceptar'
        }
          alertError(errorAlert)
    })
})



const activitySlice = createSlice({
	name: 'activities',
	initialState: {
		data: [],
		status: IDLE,
		error: null
	},
	
	extraReducers: {
		[getActivities.pending]: (state, action) => {
			state.status = LOADING;
		},
		[getActivities.fulfilled]: (state, action) => {
			state.status = SUCCEEDED;
			state.data = action.payload.data;
		},
		[getActivities.failed]: (state, action) => {
			state.status = FAILED;
			state.error = action.payload;
		}
	}
});

// Selectors for retrieving activities's slice data
export const selectActivities = state => state.activities.data;

export const latestsActivities = state => state.activities.data.slice(0,4);

export const activitiesStatus = state => state.activities.status;

// Make the reducer the default export
export default activitySlice.reducer;
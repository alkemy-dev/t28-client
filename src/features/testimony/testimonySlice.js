import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { ApiService } from '../../GeneralServices/index';
import { API_URL, TESTIMONIES_TABLE, REQUEST_METHOD_GET, IDLE, LOADING, SUCCEEDED, FAILED, REQUEST_METHOD_DELETE, errorDeleteTestimony, successDeleteTestimony } from '../../constants/index';
import { alertError, alertSuccess } from '../../components/Alert';

export const getTestimonies = createAsyncThunk(
	'testimony/getTestimonies',
	async () => {
		return await ApiService({
			API_URL: API_URL,
			BASE: TESTIMONIES_TABLE,
			METHOD: REQUEST_METHOD_GET,
		});
        
	}
);

export const deleteTestimony = createAsyncThunk(    
    'testimony/deleteTestimony',
    async (data)=>{
    return await ApiService({
        API_URL: API_URL,
        BASE: 'testimonials/'+data.id,               
        METHOD: REQUEST_METHOD_DELETE       
    })
    .then(res => {
        if(res === undefined) throw new Error(errorDeleteTestimony(data.id));
        const successAlert = {
            title: "Listo!",
            text: successDeleteTestimony(data.name),
            confirmText: 'Aceptar'
        }
          alertSuccess(successAlert)                   
          getTestimonies()              
    })
    .catch(error => {
        console.log(error)
        const errorAlert = {
            title: 'Error',
            text: error.message,
            confirmText: 'Aceptar'
        }
          alertError(errorAlert)
    })
})


const testimonySlice = createSlice({
	name: 'testimony',
	initialState: {
		data: [],
		status: IDLE
	},
	extraReducers: {
		[getTestimonies.pending]: (state, action) => {
			state.status = LOADING
		},
		[getTestimonies.fulfilled]: (state, action) => {
			state.status = SUCCEEDED;
			state.data = action.payload.data;           
		},
		[getTestimonies.rejected]: (state, action) => {
			state.status = FAILED;
		}
	}
}) 

// Selectors
export const selectTestimonies = state => state.testimony.data;

export const selectLatestTestimonies = state => state.testimony.data.slice(0,4);

export default testimonySlice.reducer;

import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { FAILED, IDLE, LOADING, SUCCEEDED } from "../../constants";
import { ApiService } from "../../GeneralServices";

export const getAllMembers = createAsyncThunk(
  "member/fetchStatus",
  async (state) => {
    return await ApiService({
      API_URL: "http://localhost:3000",
      BASE: "users",
      METHOD: "GET",
    });
  }
);

export const memberSlice = createSlice({
  name: "member",
  initialState: {
    value: [],
    status: IDLE,
  },
  reducers: {},

  extraReducers: {
    [getAllMembers.pending]: (state, action) => {
      state.status = LOADING;
    },
    [getAllMembers.fulfilled]: (state, action) => {
      state.status = SUCCEEDED;
      state.value = action.payload.users;
      console.log("el payload es : users  ", action.payload.users);
    },
    [getAllMembers.rejected]: (state, action) => {
      state.status = FAILED;
    },
  },
});

export const showValue = (state) => state.member.value;
export const showMemberStatusCall = (state) => state.member.status;

export default memberSlice.reducer;

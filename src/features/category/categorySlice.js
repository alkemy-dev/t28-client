import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { ApiService } from '../../GeneralServices';
import { API_URL, CATEGORIES_TABLE, FAILED, LOADING, REQUEST_METHOD_GET, SUCCEEDED, IDLE, REQUEST_METHOD_DELETE, successDeleteCategory, errorDeleteCategory } from '../../constants';
import { alertError, alertSuccess } from '../../components/Alert';

export const getCategories = createAsyncThunk(
    'category/getCategories',
    async() => {
        return await ApiService({
            API_URL: API_URL,
            BASE: CATEGORIES_TABLE,
            METHOD: REQUEST_METHOD_GET
        });
    }    
)

export const deleteCategory = createAsyncThunk(
    
    'category/deleteCategories',
    async (data)=>{
    return await ApiService({
        API_URL: API_URL,
        BASE: 'categories/'+data.id,               
        METHOD: REQUEST_METHOD_DELETE       
    })
    .then(res => {
        if(res === undefined) throw new Error(errorDeleteCategory(data.id));
        const successAlert = {
            title: "Listo!",
            text: successDeleteCategory(data.name),
            confirmText: 'Aceptar'
        }
          alertSuccess(successAlert)                   
                      
    })
    .catch(error => {
        console.log(error)
        const errorAlert = {
            title: 'Error',
            text: error.message,
            confirmText: 'Aceptar'
        }
          alertError(errorAlert)
    })
})


export const categorySlice = createSlice({
    name: 'category',
    initialState: {
		categories: [],
		status: IDLE
	},
	extraReducers: {
		[getCategories.pending]: (state) => {
            state.status = LOADING;
        },
        [getCategories.fulfilled]: (state, action) => {
            state.categories = action.payload.data;
            state.status = SUCCEEDED;
        },
        [getCategories.rejected]: (state) => {
            state.status = FAILED;
        }
	}
});

export default categorySlice.reducer;
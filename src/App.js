import React, { useEffect } from "react";

import { QueryClient } from "react-query";
import userProfile from "./components/userProfile/MyProfile";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
// Chakra Related
import { ChakraProvider, useDisclosure } from "@chakra-ui/react";
import { theme } from "./chakraTheme";

// Routes
import {
  HOME_ROUTE,
  ABOUT_US_ROUTE,
  USER_ROUTE,
  NEWS_ROUTE,
  ACTIVITIES_ROUTE,
  CONTACTS_ROUTE,
  LOGIN_ROUTE,
  REGISTER_ROUTE,
} from "./constants/index";

// Components
import Home from "./components/Home/index";
import BackOffice from "./components/BackOffice/BackOffice";

//Screens
import ScreenAboutUs from "./screens/aboutUs";
import ScreenActivities from "./screens/activities";
import ScreenActivity from "./screens/activity";
import ScreenContact from "./screens/contact";
import ScreenNews from "./screens/news";
import ScreenNewsDetail from "./components/News/NewsDetail";
import Testimony from "./components/Home/SelectedTestimonials";

import Layout from "./components/layout/Layout";

// Authentication
import FormLogin from "./components/authentication/FormLogin";
import FormRegister from "./components/authentication/FormRegister";
import Categories from "./components/Categories";

import ScreenListTestimonyBO from "./screens/screenListTestimonyBO";
import CategoryForm from "./components/CategoryForm";
import { useDispatch, useSelector } from "react-redux";
import { checkUser } from "./features/user/userSlice";
function App() {
  /*  const queryClient = new QueryClient(); */
  const { user } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const { onOpen, onClose, isOpen } = useDisclosure();
  useEffect(() => {
    /* check if user exist in the localstorage */

    dispatch(checkUser());
    console.log("el usuario actual es:", user);
  }, []);
  return (
    <ChakraProvider resetCSS={true} theme={theme}>
      {/* <QueryClientProvider client={queryClient}> */}
      <div className="App">
        <Router>
          <Layout>
            <Switch>
              <Route exact path={HOME_ROUTE}>
                <Home />
              </Route>
              <Route path="/nosotros">
                <ScreenAboutUs />
              </Route>
              <Route path="/novedades/:id">
                <ScreenNewsDetail />
              </Route>
              <Route path={NEWS_ROUTE}>
                <ScreenNews />
              </Route>
              <Route path="/actividades">
                <ScreenActivity />
              </Route>
              {/* <Route path={ACTIVITIES_ROUTE}>
                <ScreenActivities />
              </Route> */}

              <Route path="/testimonios">
                <Testimony />
              </Route>
              <Route path={CONTACTS_ROUTE}>
                <ScreenContact />
              </Route>
              <Route exact path={LOGIN_ROUTE}>
                <FormLogin />
              </Route>
              <Route exact path={REGISTER_ROUTE}>
                <FormRegister />
              </Route>
              <Route path={"/backoffice"}>
                <BackOffice />
              </Route>
            </Switch>
          </Layout>
        </Router>
      </div>
      {/*  </QueryClientProvider> */}
    </ChakraProvider>
  );
}

export default App;

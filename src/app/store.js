import { configureStore } from "@reduxjs/toolkit";
import counterSlice from "../features/counter/counterSlice";
import userSlice from "../features/user/userSlice";
import contactSlice from "../features/contact/contactSlice";
import newsReducer from "../features/news/newsSlice";
import categoryReducer from '../features/category/categorySlice';
import memberReducer from "../features/member/memberSlice";
import activitiesReducer from '../features/activitySlice/activitySlice';
import testimonyReducer from '../features/testimony/testimonySlice';

export default configureStore(
  {
    reducer: {
      counter: counterSlice,
      user: userSlice,
      contact: contactSlice,
      news: newsReducer,
      category: categoryReducer,
      member: memberReducer,
      activities: activitiesReducer,
      testimony: testimonyReducer
    },
  },
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
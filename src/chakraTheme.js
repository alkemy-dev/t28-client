import { extendTheme } from "@chakra-ui/react";

const colors = {
  brand: {
    900: "#db5752",
    800: "#fafa88",
    700: "#9ac9fb",
  },
};

export const theme = extendTheme({ colors });

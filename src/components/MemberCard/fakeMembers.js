import ceciliaMendez from './members/CeciliaMendez.jpeg';
import marcoFernandez from './members/MarcoFernandez.jpg';
import mariaGarcia from './members/MaríaGarcia.jpg';
import mariaIrola from './members/MaríaIrola.jpg';
import maritaGomez from './members/MaritaGomez.jpeg';
import miriamRodriguez from './members/MiriamRodriguez.jpg';
import rodrigoFuente from './members/RodrigoFuente.jpg';

const ongMembers = [
	{
		name: 'Cecilia Mendez',
		image: ceciliaMendez
	},
	{
		name: 'Marco Fernandez',
		image: marcoFernandez
	},
	{
		name: 'María García',
		image: mariaGarcia
	},
	{
		name: 'María Irola',
		image: mariaIrola
	},
	{
		name: 'Marita Gomez',
		image: maritaGomez
	},
	{
		name: 'Miriam Rodriguez',
		image: miriamRodriguez
	},
	{
		name: 'Rodrigo Fuente',
		image: rodrigoFuente
	},
]

export default ongMembers;
import React, { Fragment } from 'react';
import { 
    Box,
    Center, 
    Image   
} 
from '@chakra-ui/react';

const MemberCard = ({ data }) => {    
    
    return (
      <Fragment>
        <Box
          mx={5}
          my={5}
          h="fit-content"
          maxW="200px"
          borderWidth="1px"
          borderRadius="lg"
          overflow="hidden"
          className="bg-gray-50"
        >
          <Image
            src={data.image}
            alt={`${data.image}'s Profile Image`}
            backgroundColor="grey"
            h="200px"
            w="100%"                        
          />
          <Box p="2">
            <Center my="1" fontWeight="semibold" as="h4" lineHeight="tight">
              {data.name}
            </Center>
          </Box>
        </Box>
      </Fragment>
    );
}

export default MemberCard;
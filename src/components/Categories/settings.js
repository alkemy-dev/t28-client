export const options = {
    headerStyle: {
        backgroundColor: '#fed136',
        color: '#000',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    cellStyle: {
        textAlign: 'center'
    },
    actionsColumnIndex: -1,
    toolbarButtonAlignment:'left'
}

export const localization = {                                                                                        
    pagination: {
        firstAriaLabel: 'Primera página',
        firstTooltip: 'Primera página',
        labelDisplayedRows: '{from}-{to} de {count}',
        labelRowsPerPage: 'Filas por página:',
        labelRowsSelect: 'filas',
        lastAriaLabel: 'Ultima página',
        lastTooltip: 'Ultima página',
        nextAriaLabel: 'Pagina siguiente',
        nextTooltip: 'Pagina siguiente',
        previousAriaLabel: 'Pagina anterior',
        previousTooltip: 'Pagina anterior',
    },                            
    header: {
        actions: 'Acciones'
    },
    body: {
        emptyDataSourceMessage: 'No hay información',   
        editTooltip: 'Editar',
        deleteTooltip: 'Eliminar',
        editRow: {                                     
            deleteText: '¿Seguro desea eliminar el registro?',
            saveTooltip: 'Aceptar',
            cancelTooltip: 'Cancelar' 
        }                                
    },
    toolbar: {                                                                
        addRemoveColumns: 'Agregar o eliminar columnas',
        exportAriaLabel: 'Exportar',
        exportName: 'Exportar a CSV',
        exportTitle: 'Exportar',
        nRowsSelected: '{0} filas seleccionadas',
        searchPlaceholder: 'Buscar',
        searchTooltip: 'Buscar',
        showColumnsAriaLabel: 'Mostrar columnas',
        showColumnsTitle: 'Mostrar columnas',
    },
}

export const title = 'CATEGORÍAS'
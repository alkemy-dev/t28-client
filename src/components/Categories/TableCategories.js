import React, { useEffect, useState } from 'react';
import MaterialTable from '@material-table/core';
import { options, localization, title } from './settings';
import { getCategories, deleteCategory } from '../../features/category/categorySlice';
import { useSelector, useDispatch } from 'react-redux';
import { cloneDeep } from 'lodash';
import { Button } from '@chakra-ui/react';

import { FaPlus ,FaEdit, FaTrash } from 'react-icons/fa';
import CategoryForm from '../CategoryForm';
import { IDLE, SUCCEEDED } from '../../constants';
import LoaderComponent from '../loader/LoaderComponent';

const TableCategories = () => {
    const dispatch = useDispatch();  
    const [modal, setModal] = useState("")

    const columns = [
        {
            title: 'Nombre',
            field: 'name',
            headerStyle: { textAlign: 'left' },
            cellStyle: { textAlign: 'left' }
        },
        {                
            cellStyle: { width: '3%' },
            render: rowData => 
                <Button mr={0} colorScheme="yellow" onClick={()=>handleEditeCategory(rowData)}>
                    <FaEdit />
                </Button>
        },
        {
            cellStyle: { width: '3%' },
            render: rowData => 
                <Button colorScheme="red" onClick={()=>handleDeleteCategory({name:rowData.name, id:rowData.id})}>
                    <FaTrash />
                </Button>             
        }
    ]
 
    const handleDeleteCategory = (data)=>{
        dispatch(deleteCategory(data))
        dispatch(getCategories())
        
    }


    const handleEditeCategory = (data) =>{       
        setModal(<CategoryForm categoryObj={data} setModal={setModal}/>)
        dispatch(getCategories());        
    }    
    
    const categories = useSelector((state) => state.category.categories);    
    const status = useSelector(state => state.category.status);

    const flexData = cloneDeep(categories);

    
    useEffect(() => {
        if(status === IDLE)
            dispatch(getCategories());
               
    }, [modal, dispatch])

    return (
      <div className="md:w-11/12 xl:w-100 mx-auto mb-5">
        { status !== SUCCEEDED ? 
        <div className='flex justify-center items-center' style={{'min-height': '60vh'}}>
            <LoaderComponent />
        </div>
        :
        <>
        <MaterialTable
          columns={columns}
          data={flexData}
          title={title}
          options={options}
          localization={localization}
          actions={[
            {
              icon: () => <div style={{fontSize: '1em', color: 'rgb(40,110,251)'}}><FaPlus /></div>,
              tooltip: "Crear Usuario",
              position: "toolbar",
              onClick: () => {
                setModal(<CategoryForm setModal={setModal} />);
              }
            }
          ]}
        />
        {modal}
        </>
        }
        
      </div>
    );
}

export default TableCategories;
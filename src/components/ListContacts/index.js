import React,{useState, useEffect} from 'react';
import {
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    TableCaption,
    } from "@chakra-ui/react"
import { API_URL, USERS_CONTACTS } from '../../constants/index';
import { ApiService } from '../../GeneralServices/index';
import "./style.css";

const ListContacts = () => {
    
    const [contacts, setContacts] = useState([
            {name:"Nombre Contacto 1", age:15, info:"Información Contacto 1"},
            {name:"Nombre Contacto 2", age:15, info:"Información Contacto 2"},
            {name:"Nombre Contacto 3", age:15, info:"Información Contacto 3"},
    ]);


    const getAllContactList = async () => {
        await ApiService({API_URL: API_URL,TABLE: USERS_CONTACTS, METHOD: 'get' })
            .then( response => {if(response !== undefined) { setContacts(response) }})
            .catch(error => {
                console.log(`ERROR: ${error}`);
        })
    };

    useEffect(() => {
        getAllContactList();
    },[]);

return <>
    <div className="bg-red py-4">
        <div className="md:w-4/5 xl:w-3/5 mx-auto py-8 ">
            <Table variant="simple">
                <TableCaption textColor="white">Lista de contactos</TableCaption>
                    <Thead>
                        <Tr>
                            <Th textColor="white">Item 1</Th>
                            <Th textColor="white">Item 2</Th>
                            <Th textColor="white">Item 3</Th>
                        </Tr>
                    </Thead>
                <Tbody>
                {contacts?.map((contact,index) => {
                    return (
                    <Tr key={index}>
                        <Td textColor="white">{contact.name}</Td>
                        <Td textColor="white">{contact.age}</Td>
                        <Td textColor="white">{contact.info}</Td>
                    </Tr>
                    )
                })}
                </Tbody>
            </Table>
        </div>
    </div>
</>
}

export default ListContacts;
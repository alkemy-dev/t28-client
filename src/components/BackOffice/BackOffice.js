import React, { Fragment, useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Link, Redirect, Route, Switch, useRouteMatch } from "react-router-dom";
import { DashBoardsLinks } from "../../constants/index";
import {
  Button,
  ButtonGroup,
  Grid,
  GridItem,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
} from "@chakra-ui/react"
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AdminUsersTable from "../AdminUsersList/AdminUsersList";
import FormUpdateMyProfile from "../userProfile/FormUpdateMyProfile";
import "./backoffice.css";

// components
import NewsTable from '../News/index';
import NewsForm from '../NewsForm/index';
import ActivityTable from '../Activities/index';
import TestimonialsTable from '../../screens/screenListTestimonyBO';
import Categories from '../Categories';
import MyProfile from '../userProfile/MyProfile.js';

const BackOffice = () => {
  const initialValues = { editUser: false };
  const [links, setLinks] = useState();
  const [showItems, setShowItems] = useState(initialValues);
  const { user } = useSelector((state) => state.user);
  let { path, url } = useRouteMatch();

  const totalUsers = useSelector(state => state.user.usersList.length);
  
  // Currying function
  const activateButton = (event) => {
    for(let a of links) {
      a.firstChild.classList.remove('active');
    }
    event.target.classList.toggle('active');
  }

  useEffect(() => {
    setLinks(document.getElementById('tables').children);  
  },[])
  //VALIDATION IF THE USER EXIST
  return !user.name && !user.roleId ? (
    <Route path={path}>
      <Redirect push to="/" />
    </Route>
  ) : (
    <Fragment>
      <Grid
        h="fit-content"
        minHeight='90vh'
        templateRows="minmax(100px, auto) repeat(4, 1fr)"
        templateColumns="repeat(5, 1fr)"
        bgColor="rgb(240,240,241)"
        className="gap-x-1"
        w="100%"
      >
        <GridItem bg="" alignItems="center" className="flex justify-center align-center row-span-1 col-span-2 md:col-span-1">
          <Menu className="w-full">
            <MenuButton as={Button} rightIcon={''} className="" style={{borderRadius: '50%'}} bg="rgb(180,180,180)" minWidth="64px" minHeight="64px">
              <AccountCircleIcon />
            </MenuButton>
            
            <MenuList className="w-3/4">
              {DashBoardsLinks.map((item, index) => {
                return (
                  <MenuItem key={`${item.link}-${index}`}>     
                    <div className="dashIcon">{item.icons}</div>
                    <Link to={item.link} className="dashLink" >
                      {item.title}
                    </Link>
                  </MenuItem>
                );
              })};
            </MenuList>
          </Menu>

        </GridItem>
        
        <GridItem bg="" alignItems="center" className="flex flex-col md:flex-row row-span-1 col-span-3 md:col-span-4">

          <ButtonGroup id="tables" className="flex w-full justify-evenly flex-wrap flex-col md:flex-row" variant="outlined">

            <Button class="flex-1 mr-2">
              <Link to={`${url}/users`} 
              className="text-center block border border-blue-500 rounded text-blue-500 py-2 px-4"
              onClick={activateButton}
              >Users</Link>
            </Button>
            <Button class="flex-1 mr-2">
              <Link to={`${url}/news`} className="text-center block border border-blue-500 rounded text-blue-500 py-2 px-4"
              onClick={activateButton}>News</Link> 
            </Button>
            <Button class="flex-1 mr-2">
              <Link to={`${url}/categories`} className="text-center block border border-blue-500 rounded text-blue-500 py-2 px-4"
              onClick={activateButton}>Categories</Link> 
            </Button>
            <Button class="flex-1 mr-2">
              <Link to={`${url}/activities`} className="text-center block border border-blue-500 rounded text-blue-500 py-2 px-4"
              onClick={activateButton}>Activities</Link> 
            </Button>
            <Button class="flex-1 mr-2">
              <Link to={`${url}/testimonials`} className="text-center block border border-blue-500 rounded text-blue-500 py-2 px-4"
              onClick={activateButton}>Testimonials</Link> 
            </Button>
          </ButtonGroup>
        </GridItem>
        
        <GridItem bg="" className="md:row-span-4 col-span-5 py-5 order-1 md:col-span-1 md:order-0 items-center justify-center">
          
        </GridItem>
        
        <GridItem bg="" height="100%" maxH="100vh" minH="60vh" width="100%" className="items-center row-span-4 col-span-5 md:col-span-4 md:order-1"> 
          <Switch>
            <Route exact path={path}>Elija una tabla</Route>
            <Route path={`${path}/users`}> <AdminUsersTable/> </Route>
            <Route path={`${path}/newsForm/:id`} component={NewsForm}/>
            <Route path={`${path}/newsForm`} component={NewsForm}/>
            <Route path={`${path}/news`} component={NewsTable}/>
            <Route path={`${path}/categories`} component={Categories}/>
            <Route path={`${path}/activities`} component={ActivityTable}/>
            <Route path={`${path}/testimonials`} component={TestimonialsTable}/>
            <Route path={`${path}/profile`} component={MyProfile}/>
          </Switch>
        </GridItem>
      </Grid>

      
    </Fragment>
  );
};

export default BackOffice;

/*
  <Select>
    <option><Link to={`${url}/users`}>Users</Link></option>
    <option><Link to={`${url}/news`}>News</Link> </option>
    <option><Link to={`${url}/categories`}>Categories</Link> </option>
    <option><Link to={`${url}/activities`}>Activities</Link> </option>
    <option><Link to={`${url}/testimonials`}>Testimonials</Link> </option>
    <option></option>
  </Select>


 
  <Tabs variant="soft-rounded" colorScheme="green">
    <TabList>
      <Tab>Tab 1</Tab>
      <Tab>Tab 2</Tab>
    </TabList>
    <TabPanels>
      <TabPanel>
        <p>one!</p>
      </TabPanel>
      <TabPanel>
        <p>two!</p>
      </TabPanel>
    </TabPanels>
</Tabs>
*/

import Swal from "sweetalert2";

//Alert Confirm
export const alertConfirm = (title, text, confirmText, cancelText) => {
  Swal.fire({
    title: title,
    text: text,
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: confirmText,
    cancelButtonText: cancelText,
  }).then((result) => {
    if (result.isConfirmed) {
      Swal.fire("Deleted!", "Your file has been deleted.", "success");
    }
  });
};

//Alert Error
export const alertError = (title, text, confirmText) => {
  Swal.fire({
    icon: "error",
    title: title,
    text: text,
    confirmButtonText: confirmText,
  });
};

//Alert Information
export const alertInfo = (title, text, confirmText) => {
  Swal.fire({
    icon: "info",
    title: title,
    text: text,
    confirmButtonColor: confirmText,
  });
};

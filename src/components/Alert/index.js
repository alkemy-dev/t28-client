import Swal from "sweetalert2";

//Alert Confirm
/*
export const alertConfirm = ({
  title,
  text,
  confirmText,
  cancelText,
  confirmFunction
}) => {
  Swal.fire({
    title: title,
    text: text,
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: confirmText,
    cancelButtonText: cancelText,
  }).then((result) => {
    if (result.isConfirmed) {
      confirmFunction();
      Swal.fire(
        "Eliminado!",
        "Tu registro se eliminó correctamente.",
        "success"
      );
    }
  });
};
*/
export const alertConfirm = ({title, text, confirmText, cancelText, confirmFunction}) => {
  Swal.fire({
    title,
    text,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    confirmButtonText: confirmText,
    cancelButtonColor: "#d33",
    cancelButtonText: cancelText
  }).then(
    (result) => {
      if(result.isConfirmed) {
        confirmFunction();
      }
    })
}
//Alert Error
export const alertError = ({ title, text, confirmText }) => {
  Swal.fire({
    icon: "error",
    title: title,
    text: text,
    confirmButtonText: confirmText,
  });
};

//Alert Information
export const alertInfo = ({ title, text, confirmText }) => {
  Swal.fire({
    icon: "info",
    title: title,
    text: text,
    confirmButtonColor: confirmText,
  });
};

//Alert Success
export const alertSuccess = ({ title, text, confirmText }) => {
  Swal.fire({
    icon: "success",
    title: title,
    text: text,
    confirmButtonColor: confirmText,
  });
};

//EXAMPLE OF USE
// const alertProps = {field: value, ...}
// import {alertInfo} from "./Alert"
// alertInfo(alertProps)

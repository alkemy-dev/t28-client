import React, { useState } from "react";

import { Form, Formik } from "formik";
import {
  InputControl,
  ResetButton,
  SubmitButton,
  TextareaControl,
} from "formik-chakra-ui";
import * as Yup from "yup";

import {
  EMPTY_ENTRY_ERROR_TEXT,
  errorMaxCharacters,
  errorMinCharacters,
} from "../../constants";
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/modal";
import { ButtonGroup } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import { handleActionCategory } from "./functions";

const CategoryForm = ({ categoryObj, setModal }) => {  
  const { onOpen } = useDisclosure();
  const [initialValues] = useState(
    categoryObj ? categoryObj : { name: "", description: "" }
  );
  const type = categoryObj ? "Editar" : "Crear";
  const action = categoryObj ? "PUT" : "POST";

  const [mode, setMode] = useState(true)

  const categorySchema = Yup.object().shape({
    name: Yup.string()
      .min(2, errorMinCharacters(2))
      .max(32, errorMaxCharacters(32))
      .required(EMPTY_ENTRY_ERROR_TEXT),
    description: Yup.string()
      .min(8, errorMinCharacters(8))
      .max(200, errorMaxCharacters(200))
      .required(EMPTY_ENTRY_ERROR_TEXT),
  });

  const onSubmit = async (data) => {
      handleActionCategory(action, data);
      setMode(false)
      setModal("")
  };

  const onClose = ()=>{
      setMode(false)
      setModal("")
  }

  return (
    <>
      {/* <Button onClick={onOpen}>Open Modal</Button> */}
      <Modal isOpen={mode} onClose={onClose} onOpen={onOpen}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{type} Categoria</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik
              initialValues={initialValues}
              onSubmit={onSubmit}
              validationSchema={categorySchema}
            >
              {
                <Form>
                  <InputControl name="name" label="Nombre" />
                  <TextareaControl name="description" label="Descripcion" />
                  <ButtonGroup>
                    <SubmitButton>Confirmar</SubmitButton>
                    <ResetButton>Reset</ResetButton>
                  </ButtonGroup>
                </Form>
              }
            </Formik>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default CategoryForm;

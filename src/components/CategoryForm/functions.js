import { ApiService } from "../../GeneralServices";
import { alertError, alertSuccess } from "../Alert";

import {API_URL} from "../../constants";


export const handleActionCategory = (action, data)=>{
    console.log("info del form: "+ data.name)
    console.log("action "+ action)
    const dataQuery = {
        "name": data.name,
        "description": data.description,
        "updateAt": new Date()
      }
    ApiService({
      API_URL: API_URL,
      BASE: action === "POST" ? "categories" : "categories/" + data.id,
      METHOD: action,
      data: dataQuery  
    })
      .then((res) => {
        const mode = action === "POST" ? "creo" : "edito";
        if (res === undefined)
          throw new Error("Error al editar o crear la categoria " + data.name);
        const successAlert = {
          title: "Categoria",
          text: `La categoria ${data.name} se ${mode} correctamente`,
          confirmText: "Aceptar",
        };
        alertSuccess(successAlert);
      })
      .catch((error) => {
        console.log(error);
        const errorAlert = {
          title: "Error",
          text: error.message,
          confirmText: "Aceptar",
        };
        alertError(errorAlert);
      });
}
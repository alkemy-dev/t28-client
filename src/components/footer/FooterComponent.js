import React from "react";

import { Link as ReactLink } from "react-router-dom";

import { Box, Center, Link } from "@chakra-ui/react";
import "./footer.css";


//Icons import
import Twitter from "../../assets/icons/Twitter";
import Facebook from "../../assets/icons/Facebook";
import Instagram from "../../assets/icons/Instagram";
import Whatsapp from "../../assets/icons/Whatsapp";

const linksMockUp = [
  { text: "Noticias", url: "/noticias" },
  { text: "Actividades", url: "/actividades" },
  { text: "Novedades", url: "/novedades" },
  { text: "Testimonios", url: "#" },
  { text: "Nosotros", url: "/nosotros" },
  { text: "Contacto", url: "/contactos" },
];

const socialLinksMockUp = [
  { network: Twitter, url: "http://twitter.com" },
  { network: Facebook, url: "http://facebook.com" },
  { network: Instagram, url: "http://instagram.com" },
  { network: Whatsapp, url: "http://whatsapp.com" },
];

const FooterComponent = () => {

  const logo = "/logo.png"
  
  return (
    <footer className="contentFooter">
      <Box className="navFooter">
        {linksMockUp.map((link) => (
          <Link key={link.text} as={ReactLink} to={link.url} my={5} mx={8}>
            {link.text}
          </Link>
        ))}
      </Box>
      <Center>
          <img className="ong-logo" src={logo} title="ONG-LOGO" alt="ONG-LOGO" />
      </Center>
      <Center className="iconsFooter">
        {socialLinksMockUp.map((link) => (
          <Link
            key={link.network}
            fontSize="xl"
            fontWeight={500}
            fontFamily="heading"            
            m={5}
            href={link.url}
            isExternal
          >
            <link.network />
          </Link>
        ))}
      </Center>
      <Center className="rightsFooter"> 2021 by Alkemy. All Rights Reserved. </Center>
    </footer>
  );
};

export default FooterComponent;

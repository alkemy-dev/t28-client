import React from "react";
import { useMemo } from "react";
import { API_URL, REQUEST_METHOD_GET } from "../../constants";
import { ApiService } from "../../GeneralServices";
import FooterComponent from "./FooterComponent";

//Icons import
import Twitter from "../../assets/icons/Twitter";
import Facebook from "../../assets/icons/Facebook";
import Instagram from "../../assets/icons/Instagram";
import Whatsapp from "../../assets/icons/Whatsapp";

const Footer = () => {
  // /organizations/:id/public
  const PUBLIC_URL = "/organizations/1/public";

  const data = useMemo(
    () =>
      ApiService({
        API_URL: API_URL,
        BASE: PUBLIC_URL,
        METHOD: REQUEST_METHOD_GET,
      })
        .then((res) => {
          console.log(res);
        })
        .catch((error) => {
          console.log(error);
        }),
    []
  );

  const linksMockUp = [
    { text: "Inicio", url: "#" },
    { text: "Nosotros", url: "#" },
    { text: "Actividades", url: "#" },
    { text: "Novedades", url: "#" },
    { text: "Testimonios", url: "#" },
    { text: "Contacto", url: "#" },
    { text: "Contribuye", url: "#" },
  ];

  const socialLinksMockUp = [
    { network: Twitter, url: "#" },
    { network: Facebook, url: "#" },
    { network: Instagram, url: "#" },
    { network: Whatsapp, url: "#" },
    { network: Linkedin, url: "#" },
  ];

  return (
    <FooterComponent
      linksMockUp={linksMockUp}
      socialLinksMockUp={socialLinksMockUp}
    />
  );
};

export default Footer;

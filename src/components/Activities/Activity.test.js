import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Activities from './index';

configure({ adapter: new Adapter() });

describe('Activities', () => {
    test('matches snapshot', ()=> {
        const wrapper = shallow(<Activities />);
        expect(wrapper).toMatchSnapshot();
    });
});
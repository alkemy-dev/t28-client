import React, { useState, useEffect } from 'react';
import MaterialTable, { MTableToolbar } from '@material-table/core';

import { options, localization, title } from './settings';
import { useSelector, useDispatch } from 'react-redux';
import { selectActivities, getActivities, deleteActivities } from '../../features/activitySlice/activitySlice';

import { cloneDeep } from 'lodash';
import { Button } from '@chakra-ui/button';
import { FaPlus ,FaEdit, FaTrash } from 'react-icons/fa';

import ActivityForm from '../ActivityForm/ActivityForm';
import LoaderComponent from '../loader/LoaderComponent';
import { IDLE, SUCCEEDED } from '../../constants';

const TableActivities = () => {
    
    const dispatch = useDispatch();
    const [modal, setModal] = useState("")
    const columns = [
        {
            title: 'Nombre',
            field: 'name',                
            headerStyle: { textAlign: 'left' },        
            cellStyle: { textAlign: 'left' }          
        },
        {                
            cellStyle: { width: '3%' },
            render: rowData => 
                <Button onClick={() => handleEditeActivity(rowData)} mr={0} colorScheme="yellow">
                    <FaEdit />
                </Button>
        },
        {
            cellStyle: { width: '3%' },
            render: rowData => 
                <Button onClick={() => handleDeleteActivity({name:rowData.name, id:rowData.id})} colorScheme="red">
                    <FaTrash />
                </Button>             
        }
    ]

    const handleDeleteActivity = (dataAct)=>{
        dispatch(deleteActivities(dataAct))
        dispatch(getActivities())        
    }


    const handleEditeActivity = (dataAct) =>{       
        setModal(<ActivityForm activity={dataAct} setModal={setModal}/>)
        dispatch(getActivities());        
    }    

    const data = useSelector(selectActivities); // <- get data from redux store
    const flexData = cloneDeep(data);

    const activitiesStatus = useSelector(state => state.activities.status);

    useEffect(() => { 
        if( activitiesStatus === IDLE)      
            dispatch(getActivities());        
    }, [dispatch, modal]);

    return(
        <div className="md:w-11/12 xl:w-100 mx-auto mb-5"> 
            { activitiesStatus !== SUCCEEDED ? 
        <div className='flex justify-center items-center' style={{'min-height': '60vh'}}>
            <LoaderComponent />
        </div>
        :
        <>
            <MaterialTable                                
                columns={columns}     
                data={flexData}                  
                title={title}
                options={options}                                                                                         
                localization={localization}
                components={{
                    Toolbar: props => (
                        <div style={{ flexWrap: 'wrap', flex:1}}>
                            <MTableToolbar style={{flexWrap: 'wrap'}} {...props} />
                        </div>
                    )
                }}
                actions={[
            {
              icon: () => <div style={{fontSize: '1em', color: 'rgb(40,110,251)'}}><FaPlus /></div>,
              tooltip: "Crear Usuario",
              position: "toolbar",
              onClick: () => {
                setModal(<ActivityForm setModal={setModal} />);
              }
            }
          ]}                            
            />
            {modal}
            </>}
        </div>
    )
}

export default TableActivities;
import React, { useState } from "react";
import { Form, Formik } from "formik";
import { ButtonGroup } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/modal";

import * as Yup from "yup";

import { InputControl, ResetButton, SelectControl, SubmitButton } from "formik-chakra-ui";

//Utils
import { newsSchema, emptyValues } from "./utils";

//Functions
import { onSubmit } from "./functions";

//Constants
import {
  EMPTY_ENTRY_ERROR_TITLE,
  EMPTY_ENTRY_ERROR_TEXT,
  EMPTY_ENTRY_ERROR_CONFIRM_TEXT,
} from "../../constants";

//Components
import CKEditor from "./CKEditor";
import { alertError } from "../Alert";
import { stringify } from "postcss";


const NewsForm = ({newsObject, setModal}) => {
  
  const { onOpen } = useDisclosure();
  const [mode, setMode] = useState(true)
  const type = newsObject ? "Editar" : "Crear";

  const onClose = ()=>{
    setMode(false)
    setModal("")
}

// const newsSchema = Yup.object({
//   name: Yup.string().required("Ingrese un titulo"),  
//   image: Yup.string().required("Ingrese una imagen"),
//   type: Yup.string().required("Ingrese un tipo"),
// });

  const [initialValues] = useState(
    newsObject ? newsObject : emptyValues
  );

  const [content, setContent] = useState(initialValues.content)
  
  
  const handleSubmitFormik = (data) => {
   
    console.log("content", content)   
    
    console.log("Submit en form",data)
    //Create Form Data Object
    const formData = data;
    formData.content = content;
    if (!newsObject) {           
      onSubmit({ formData })
    } else {
     
      onSubmit({ newsObject, formData });  
    }
    setMode(false)
    setModal("")
  };

  return (
    <>
      <Modal size="xl" isOpen={mode} onClose={onClose} onOpen={onOpen}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{type} Entrada</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik
              initialValues={initialValues}
              onSubmit={handleSubmitFormik}
              validationSchema={newsSchema}
            >
              {
                <Form>
                  <InputControl m="25px auto" name="name" label="Titulo" />

                  <InputControl m="25px auto" name="image" label="Imagen" />

                  <SelectControl
                    name="type"
                    m="25px auto"
                    label="Tipo de Entrada"
                    selectProps={{ placeholder: "Seleccionar Tipo" }}
                  >
                    <option value="event">Evento</option>
                    <option value="news">Noticia</option>
                  </SelectControl>

                  <CKEditor                    
                    content={content}
                    setContent={setContent}
                  />

                  <ButtonGroup m="25px auto">
                    <SubmitButton>
                      {newsObject ? "Editar" : "Crear"} entrada
                    </SubmitButton>
                    <ResetButton>Reset</ResetButton>
                  </ButtonGroup>
                </Form>
              }
            </Formik>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  ); 
};

export default NewsForm;

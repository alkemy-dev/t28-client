import * as Yup from "yup";
import { EMPTY_ENTRY_ERROR_TEXT, errorMaxCharacters, errorMinCharacters } from "../../constants";

//Form Validation Schema
export const newsSchema = Yup.object({
  name: Yup.string()
  .min(2, errorMinCharacters(2))
  .max(32, errorMaxCharacters(32))
  .required(EMPTY_ENTRY_ERROR_TEXT), 
  image: Yup.string().required("Ingrese una imagen"),
  type: Yup.string()
  .min(2, errorMinCharacters(2))
  .max(32, errorMaxCharacters(32))
  .required(EMPTY_ENTRY_ERROR_TEXT),
});

//Form Empty Values
export const emptyValues = {
  name: "",
  image: "",
  type: "",
  content: ""
};

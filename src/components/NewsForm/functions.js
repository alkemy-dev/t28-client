import { ApiService } from "../../GeneralServices";
import { alertError, alertSuccess } from "../Alert";

import {  
  REQUEST_METHOD_POST,
  CREATE_ENTRY_ERROR_TITLE,
  CREATE_ENTRY_ERROR_TEXT,
  CREATE_ENTRY_ERROR_CONFIRM_TEXT,
  API_URL,
} from "../../constants";

export const onSubmit = ({ newsObject, formData }) => {
  console.log("Hello from onSubmit /functions.js");
  console.log("desde function newsform: ", formData);
  
  const error = {
    title: CREATE_ENTRY_ERROR_TITLE,
    text: CREATE_ENTRY_ERROR_TEXT,
    confirmText: CREATE_ENTRY_ERROR_CONFIRM_TEXT,
  };

  const options = {
    API_URL: API_URL,
    METHOD: REQUEST_METHOD_POST,
    data: formData,
    BASE: "news",
  };

  if (newsObject) {
    options.METHOD = 'PUT';
    options.TABLE = newsObject.id;
  }

  ApiService(options)
    .then((res) => {
      console.log(res);
      if (!res)
        throw new Error("Error al editar o crear la entrada " + formData.name);
      newsObject ? console.log("editando...") : console.log("creando")      
    })
    .then(() => {
      const successAlert = {
        title: "Exito!",
        text: `La operacion fue confirmada`,
        confirmText: "Aceptar",
      };
      alertSuccess(successAlert);
    })
    .catch((err) => {
      alertError(error);
      console.log(err);
    });
};

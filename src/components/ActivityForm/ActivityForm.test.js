import React from "react";
import { shallow } from "enzyme";
import ActivityForm from "./ActivityForm";

describe("ActivityForm", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<ActivityForm />);
    expect(wrapper).toMatchSnapshot();
  });
});

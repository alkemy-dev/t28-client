import React, { useState } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import * as Yup from "yup";
import { useFormik } from "formik";
import {
  FormControl,
  FormLabel,
  Input,
  Box,  
  ButtonGroup,
  Button,
  Text,
  Heading,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
} from "@chakra-ui/react";
import { EMPTY_VALUE_MESSAGE } from "../../constants";
import { handleAction } from "./functions";
import { getActivities } from "../../features/activitySlice/activitySlice";
import { useDispatch } from "react-redux";

// Redirected from <Redirect push to={{pathname:"/actividades/edit", state: {activity: rowData}}} />
const ActivityForm = ({ activity, setModal }) => {
  const { onOpen } = useDisclosure();
  const dispatch = useDispatch()

  const [mode, setMode] = useState(true)
  const action = activity ? "PUT" : "POST";

  const onClose = ()=>{
    setMode(false)
    setModal("")
}

  /*SET INITIAL VALUES FROM AN ATTRIBUTE */
  const initialValues = {
    name: activity ? activity.name : "",
    content: activity ? activity.content : "",
    id: activity ? activity.id : "",
  };
  /*VALIDATION SCHEMA */
  const validationSchema = Yup.object({
    name: Yup.string().required(EMPTY_VALUE_MESSAGE),
    content: Yup.string().required(EMPTY_VALUE_MESSAGE),
  });
  /* FORMIK PREPARATION */
  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: validationSchema,
    onSubmit: (values) => {
      /* console.log("los valores son", values); */
      handlerPostPutActivities(values);
    },
  });
  /* SET FORMIK CONTENT VALUE */
  const handleChange = (e, editor) => {
    const data = editor.getData();
    formik.setFieldValue("content", data);
  };

  const handlerPostPutActivities = (data) => {
    handleAction(action, data);
    setMode(false)
    setModal("")
    dispatch(getActivities())
  };

  return (
    <Modal isOpen={mode} onClose={onClose} onOpen={onOpen}>
    <ModalOverlay />
    <ModalContent>
      <ModalHeader>
      {activity ? (
            <Heading
              as="h4"
              size="md"
              bg="yellow.200"
              borderRadius="3xl"
              textAlign="center"
            >
              Actualizar Información De Actividad
            </Heading>
          ) : (
            <Heading as="h4" size="md" borderRadius="3xl" textAlign="center">
              Agregar Actividad
            </Heading>
          )}
      </ModalHeader>
      <ModalCloseButton />
      <ModalBody>
    
      <Box padding="4" maxW="3xl" borderRadius="2xl">
        <FormControl borderRadius="2xl" color="GrayText">          
          <form onSubmit={formik.handleSubmit}>
            <FormLabel htmlFor="name" mt="5">
              <strong> Titulo</strong>
            </FormLabel>
            <Input
              type="text"
              name="name"
              id="name"
              defaultValue={initialValues.name}
              onChange={formik.handleChange}
              mb="5"
            />
            {formik.touched.name && formik.errors.name ? (
              <Text color="tomato" bg="orange.50">
                {formik.errors.name}
              </Text>
            ) : null}

            <CKEditor
              editor={ClassicEditor}
              data={initialValues.content}
              onChange={handleChange}
            />
            {formik.touched.content && formik.errors.content ? (
              <Text color="tomato" bg="orange.50">
                {formik.errors.content}
              </Text>
            ) : null}
            <ButtonGroup mt="5">
            <Button type="submit" bg="#319795" color="white">
              {activity ? "Actualizar" : "Crear"}
            </Button>  
              
            </ButtonGroup>
          </form>
        </FormControl>
      </Box>
    
    </ModalBody>
        </ModalContent>
      </Modal>
  );
};

export default ActivityForm;

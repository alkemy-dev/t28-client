import React from "react";
import { styles } from './classNames';
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { loginSuccess } from '../../features/user/userSlice';

// Functions
import { handleRegister } from "./functions";

import { validationForm } from './utils';

const FormRegister = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const schema = validationForm;

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (user) => {
    handleRegister(user, dispatch);
    history.push('/');
  };

  return (
    <div className="md:w-4/5 xl:w-3/5 mx-auto">
      <h2 className={styles.classTitle}>Crear Cuenta</h2>

      <div className="flex justify-center mt-5">
        <div className="w-full max-w-lg">
          <form className={styles.classForm} onSubmit={handleSubmit(onSubmit)}>
            <div className="mb-4">
              <label className={styles.classLabel} htmlFor="firstName">
                Nombre
              </label>
              <input
                type="text"
                className={styles.classInput}
                placeholder="Nombre de Usuario"
                {...register("firstName")}
              />
              {errors.firstName && (
                <div className={styles.classValidation}>{errors.firstName?.message}</div>
              )}
            </div>

            <div className="mb-4">
              <label className={styles.classLabel} htmlFor="lastName">
                Apellido
              </label>
              <input
                type="text"
                className={styles.classInput}
                placeholder="Apellido de Usuario"
                {...register("lastName")}
              />
              {errors.lastName && (
                <div className={styles.classValidation}>
                  {errors.lastName?.message}
                </div>
              )}
            </div>

            <div className="mb-4">
              <label className={styles.classLabel} htmlFor="email">
                Email
              </label>
              <input
                type="email"
                className={styles.classInput}
                placeholder="Email de Usuario"
                {...register("email")}
              />
              {errors.email && (
                <div className={styles.classValidation}>{errors.email?.message}</div>
              )}
            </div>

            <div className="mb-4">
              <label className={styles.classLabel} htmlFor="password">
                Password
              </label>
              <input
                type="password"
                className={styles.classInput}
                placeholder="Password de Usuario"
                {...register("password")}
              />
              {errors.password && (
                <div className={styles.classValidation}>
                  {errors.password?.message}
                </div>
              )}
            </div>

            <div className="mb-4">
              <label className={styles.classLabel} htmlFor="confirmPassword">
                Confirmar Password
              </label>
              <input
                type="password"
                className={styles.classInput}
                placeholder="Repite la Password"
                {...register("confirmPassword")}
              />
              {errors.confirmPassword && (
                <div className={styles.classValidation}>
                  {errors.confirmPassword?.message}
                </div>
              )}
            </div>

            <div className="grid grid-cols-1 divide-y divide-gray-400">
              <input
                type="submit"
                className={styles.classButton}
                value="Crear Cuenta"
                onClick={() => handleSubmit()}
              />

              <div className="text-center">                
                <Link 
                  to="/login"
                  className={styles.classLink}>
                  Iniciar sesión en una cuenta existente
                </Link>                
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default FormRegister;

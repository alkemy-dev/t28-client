import * as Yup from "yup"; 

//Constants
import { 
    EMAIL_INVALID, 
    FIELD_REQUIRED,
    PASSWORD_ERROR,
    errorMinCharacters
} from "../../constants";

export const validationForm = 
  Yup.object({
    firstName: Yup
      .string()
      .required(FIELD_REQUIRED),
    lastName: Yup
      .string()
      .required(FIELD_REQUIRED),
    email: Yup
      .string()
      .email(EMAIL_INVALID)
      .required(FIELD_REQUIRED),
    password: Yup
      .string()
      .required(FIELD_REQUIRED)
      .min(6, errorMinCharacters(6)),
    confirmPassword: Yup
      .string()
      .required(FIELD_REQUIRED)
      .min(6, errorMinCharacters(6))
      .oneOf([Yup.ref("password"), null], PASSWORD_ERROR),
  });
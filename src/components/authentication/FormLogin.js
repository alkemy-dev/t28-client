import React from "react";
import { styles } from "./classNames";
import { useFormik } from "formik";
import { useHistory, Link, Redirect, Route } from "react-router-dom";

// Functions
import { handleLogin } from "./functions";

// Utils
import { validationForm } from "./utils";
import { useDispatch, useSelector } from "react-redux";
import { loginSuccess, loginUser } from "../../features/user/userSlice";
import { Button } from "@material-ui/core";
import { API_URL } from "../../constants";
import { alertError, alertSuccess } from "../Alert";

const FormLogin = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state.user);

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
      
    },
    validationSchema: validationForm,
    onSubmit: (formData) => {
      console.log("me tocaste!!!!");
      //handleLogin(formData, history);
      dispatch(loginUser(formData));
    },
  });
  const cargar = async (e) => {
    const user = {
      email: document.querySelector("#email").value,
      password: document.querySelector("#password").value,
    };
    try {
      const res = await fetch(API_URL + "/auth/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(user),
      });
      const data = await res.json();
      console.log("los datos cargados son: ", data);
      dispatch(loginSuccess(data));
      if (data.status === 200) {
        alertSuccess({ title: "Bienvenido " + data.user.firstName });
        history.push("/");
      } else {
        alertError({ title: "Usuario no encontrado", confirmText: "Cerrar" });
      }
    } catch (error) {
      console.log("huboi un error", error);
    }
  };

  return user.name && user.roleId ? (
    <Route exact path="/login">
      <Redirect push to="/" />
    </Route>
  ) : (
    <div className="md:w-4/5 xl:w-3/5 mx-auto">
      <h2 className={styles.classTitle}>Iniciar Sesión</h2>

      <div className="flex justify-center mt-5">
        <div className="w-full max-w-lg">
          <form onSubmit={formik.handleSubmit}>
            <div className="mb-4">
              <label className={styles.classLabel} htmlFor="email">
                Email
              </label>
              <input
                type="email"
                name="email"
                id="email"
                onChange={formik.handleChange}
                value={formik.values.email}
                className={styles.classInput}
                placeholder="Email de Usuario"
              />
              {formik.errors.email && (
                <div className={styles.classValidation}>
                  {formik.errors.email}
                </div>
              )}
            </div>
            <div className="mb-4">
              <label className={styles.classLabel} htmlFor="password">
                Password
              </label>
              <input
                type="password"
                name="password"
                id="password"
                onChange={formik.handleChange}
                value={formik.values.password}
                className={styles.classInput}
                placeholder="Password de Usuario"
              />
              {formik.errors.password && (
                <div className={styles.classValidation}>
                  {formik.errors.password}
                </div>
              )}
            </div>

            <div className="grid grid-cols-1 divide-y divide-gray-400">
              {/* <Button
                type="submit"
                  className={styles.classButton} 
                onClick={formik.handleSubmit}
              > 
                enviar
              </Button>*/}
              <button
                type="button"
                className={styles.classButton}
                onClick={cargar}
              >
                TOCARR
              </button>

              <div className="text-center">
                <Link to="/register" className={styles.classLink}>
                  Si no tienes cuenta, Regístrate
                </Link>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default FormLogin;

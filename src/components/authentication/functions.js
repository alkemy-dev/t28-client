import { ApiService } from "../../GeneralServices";
import axios from 'axios';
//Alert Related
import { alertError } from "../Alert/index";
// Import Redux to persist data
import { loginSuccess } from '../../features/user/userSlice';
//Constants
import { 
  REQUEST_METHOD_POST, 
  API_URL,
  USERS_TABLE,
  AUTH_TABLE,
  AUTH_REGISTER,
  AUTH_LOGIN
} from "../../constants/index";

export const handleLogin = ({ email, password }, history) => {        
  const error = {
    title: "Error de autenticacion",
    text: "No se ha podido iniciar sesion",
    confirmText: "Cerrar",
  };

  ApiService({
    API_URL: API_URL,
    METHOD: REQUEST_METHOD_POST,
    data: { email, password },
    BASE: AUTH_TABLE,
    TABLE: AUTH_LOGIN,
  })
    .then((res) => {
      const { ok } = res.json();

      //If status != 200 or error Throws Alert
      if (res.status !== 200 || ok === false) {
        alertError(error);
      } else {
        //If logged correct, redirects to home.
        history.push("/");
      }
    })
    .catch((err) => {
      const error = {
        title: "Error de autenticacion",
        text: "No se ha podido iniciar sesion",
        confirmText: "Cerrar",
      };
      alertError(error);
      console.log(err);
    });
};
// This register function receives an already logged in user and token
export const handleRegister = async ({ 
  firstName, 
  lastName, 
  email, 
  password, 
  roleId = 1, 
  organizationId = 1 }, dispatch) => {
  // ApiService({
  //   API_URL: API_URL,
  //   METHOD: REQUEST_METHOD_POST,
  //   data: { firstName, lastName, email, password },
  //   BASE: "users/auth",
  //   TABLE: "register",
  // });

  var config = {
    method: REQUEST_METHOD_POST,
    url: `${API_URL}/${USERS_TABLE}/${AUTH_TABLE}/${AUTH_REGISTER}`,
    data : {firstName, lastName, email, password, roleId, organizationId}
  };

  await axios(config)
  .then(function (response) {
    dispatch(loginSuccess(response.data));
    return response.data;
  })
  .catch(function (error) {
    console.log('Axios ERROR ',error);
  });

};
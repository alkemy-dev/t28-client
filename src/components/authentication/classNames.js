export const styles = {    
    classForm: `bg-white rounded shadow-md px-8 pt-6 pb-8 mb-20`,
    classTitle: `text-3xl font-sans font-bold text-black-500 text-center my-4`,
    classLabel: `block text-black text-sm font-bold mb-2`,
    classInput: `shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`,
    classValidation: `my-2 bg-gray-200 border-l-4 border-blue-500 text-blue-700 p-3`,
    classButton: `bg-blue-400 hover:bg-gray-900 w-full p-2 text-white uppercase font-bold mt-3 mb-6 cursor-pointer`,
    classLink: `text-gray-800 text-sm-1 cursor-pointer`,    
}
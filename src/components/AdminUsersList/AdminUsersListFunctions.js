import React from "react";
import { ApiService } from "../../GeneralServices/index";
import { API_URL, USERS_TABLE } from "../../constants/index";

const userFunctions = {};

// Edit User
userFunctions.editUser = (id) => {
  console.log("edit user", id);
};

// Delete User
userFunctions.deleteUser = async (id) => {
  console.log("delete user", id);
  //if confirmation is received
  await ApiService({
    API_URL: API_URL,
    TABLE: USERS_TABLE,
    ID: id,
    METHOD: "delete",
  }).then(
    (response) => console.log(response),
    (err) => console.log(err)
  );
};

export default userFunctions;

import { Button } from '@chakra-ui/react';
import { handleEditUser, handleDeleteUser } from './functions'

export const columns = [
    {
            title: 'Nombre',
            field: 'firstName',
            headerStyle: { textAlign: 'left' },
            cellStyle: { textAlign: 'left' }
        },
        {
            title: 'Apellido',
            field: 'lastName',
            headerStyle: { textAlign: 'left' },
            cellStyle: { textAlign: 'left' }
        },
        {
            title: 'Email',
            field: 'email',
            headerStyle: { textAlign: 'left' },
            cellStyle: { textAlign: 'left' }
        },
        {
            title: 'Rol',
            field: 'roleId',
            headerStyle: { textAlign: 'left' },
            cellStyle: { textAlign: 'left' }
        },
        {
            title: 'Organizacion',
            field: 'organizationId',
            headerStyle: { textAlign: 'left' },
            cellStyle: { textAlign: 'left' }
        },
        {                
            cellStyle: { width: '3%' },
            render: rowData => 
                <Button mr={0} colorScheme="yellow" onClick={()=>handleEditUser(rowData)}>
                    <FaEdit />
                </Button>
        },
        {
            cellStyle: { width: '3%' },
            render: rowData => 
                <Button colorScheme="red" onClick={()=>handleDeleteUser({name:rowData.name, id:rowData.id})}>
                    <FaTrash />
                </Button>             
        }
]
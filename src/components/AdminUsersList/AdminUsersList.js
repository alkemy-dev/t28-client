import React, { Fragment, useEffect, useState } from "react";
import {
    Grid,
  Button,
  ButtonGroup,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  chakra,
} from "@chakra-ui/react";
import { alertConfirm } from '../Alert';
// table config
import MaterialTable, {MTableToolbar} from '@material-table/core';
import { options, localization, title } from './settings';
import { FaPlus ,FaEdit, FaTrash } from 'react-icons/fa';
// redux store
import { useSelector, useDispatch } from 'react-redux';
import { getUsers, deleteUser } from '../../features/user/userSlice';
import { cloneDeep } from 'lodash';

import userFunctions from "./AdminUsersListFunctions";
import UserEditForm from '../UserEditForm/UserEditForm';
import { IDLE, SUCCEEDED } from '../../constants/index';
import LoaderComponent from '../loader/LoaderComponent';


const UsersTable = () => {
    const dispatch = useDispatch();  
    const [modal, setModal] = useState("")
    

    const columns = [
        {
            title: 'Nombre',
            field: 'firstName',
            headerStyle: { textAlign: 'left' },
            cellStyle: { textAlign: 'left' }
        },
        {
            title: 'Apellido',
            field: 'lastName',
            headerStyle: { textAlign: 'left' },
            cellStyle: { textAlign: 'left' }
        },
        {
            title: 'Email',
            field: 'email',
            headerStyle: { textAlign: 'left' },
            cellStyle: { textAlign: 'left' }
        },
        {
            title: 'Rol',
            field: 'roleId',
            headerStyle: { textAlign: 'left' },
            cellStyle: { textAlign: 'left' }
        },
        {
            title: 'Organizacion',
            field: 'organizationId',
            headerStyle: { textAlign: 'left' },
            cellStyle: { textAlign: 'left' }
        },
        {                
            cellStyle: { width: '3%' },
            render: rowData => 
                <Button mr={0} colorScheme="yellow" onClick={()=>handleEditUser(rowData)}>
                    <FaEdit />
                </Button>
        },
        {
            cellStyle: { width: '3%' },
            render: rowData => 
                <Button colorScheme="red" onClick={()=>handleDeleteUser(rowData)}>
                    <FaTrash />
                </Button>             
        }
    ]
 
    const handleDeleteUser = (data)=>{
        alertConfirm({
            title: 'Eliminar', 
            text: '¿Desea eliminar el usuario?',
            confirmText: 'Eliminar', 
            cancelText: 'Cancelar', 
            confirmFunction: function() {dispatch(deleteUser(data))}
        });
        
    }


    const handleEditUser = (data) =>{       
        setModal(<UserEditForm userObj={data} setModal={setModal}/>)
    }    
    
    const userStatus = useSelector(state => state.user.status);
    const users = useSelector((state) => state.user.usersList);    
    const flexData = cloneDeep(users);

    
    useEffect(() => {
      if(userStatus === IDLE) {
            dispatch(getUsers());
      }
    }, [userStatus, dispatch])

    return (
      <div className="md:w-11/12 xl:w-100 mx-auto mb-5">
        { userStatus !== SUCCEEDED ? 
        <div className='flex justify-center items-center' style={{'min-height': '60vh'}}>
            <LoaderComponent />
        </div>
        :
        <>
        <MaterialTable
          columns={columns}
          data={flexData}
          title={title}
          options={options}
          localization={localization}
          actions={[
            {
              icon: () => <div style={{fontSize: '1em', color: 'rgb(40,110,251)'}}><FaPlus /></div>,
              tooltip: "Crear Usuario",
              position: "toolbar",
              onClick: () => {
                setModal(<UserEditForm setModal={setModal} />);
              }
            }
          ]}
        />
        {modal}
        </>}
      </div>
    );
}

export default UsersTable;

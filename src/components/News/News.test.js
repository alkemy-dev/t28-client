import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import News from './index';

configure({ adapter: new Adapter() });

describe('News', () => {
    test('matches snapshot', ()=> {
        const wrapper = shallow(<News />);
        expect(wrapper).toMatchSnapshot();
    });
});
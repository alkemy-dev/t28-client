import React from "react";
import ReactDOM from "react-dom";
import TableNews from "./TableNews";
import {render, cleanup} from '@testing-library/react';

afterEach(cleanup);

it('Tests if Title and columns displays correctly', () => {
	const { getByText } = render(<TableNews />);

	// Test if Table columns and Title displays correctly
	expect(getByText(/Noticias/i).textContent).toBe("NOTICIAS");
  expect(getByText(/Imagen/i).textContent).toBe("Imagen");
  expect(getByText(/Nombre/i).textContent).toBe("Nombre");
  expect(getByText(/Fecha/i).textContent).toBe("Fecha de creación");
});




/*
	Mount/render is typically used for integration testing and shallow is used for unit testing.

	shallow rendering only renders the single component we are testing. It does not render child components. 
	This allows us to test our component in isolation.

	Snapshot testing actually test the syntax of our component and the changes on the component structure.



*/
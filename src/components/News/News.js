import React from 'react';
import NewsDetail from './NewsDetail';
import { alertError } from '../Alert/AlertTypes';

const News = ({news}) => {
    
    if(Object.keys(news).length === 0) return (
        <div>{alertError('Error', 'El elemento indicado no existe', 'Ok')}</div>
    );

    return (
        <div className="md:w-4/5 xl:w-2/5 mx-auto">
            <NewsDetail news={news} />
        </div>
    );
}
 
export default News;
import React, { useEffect } from 'react';
import "../News/index.sass";
import { useParams } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import { getNews, selectNews } from '../../features/news/newsSlice';
import { IDLE } from '../../constants';

const NewsDetail = () => {

  const data = useSelector(selectNews);
  const dispatch = useDispatch();

  const error404 = {
    name: "Error",
    content: "No se encuentra esa novedad",
    image: ""
  }

  const idNovedad = useParams();
  console.log(parseInt(idNovedad.id))
  const newsStatus = useSelector(state => state.news.status);

  useEffect(() => {
      if( newsStatus === IDLE && data.length === 0) {
          dispatch(getNews());
      }
  }, [newsStatus]);

    const itemNovedad = data.filter((item)=> parseInt(item.id) === parseInt(idNovedad.id));
    console.log(itemNovedad)
    const { name, content, image } = itemNovedad[0] === undefined ? error404 : itemNovedad[0];
    

    return (
        <div className="screen" id="news-detail-screen">
        <div className="header">
          <div className="container">
            <img src={image} className="mx-auto w-full pb-10"  alt="newsImage"/>
            <div className="name pb-2">{name}</div>
            <div className="content">
              <h2>{content.replace(/(<([^>]+)>)/ig, '')}</h2>
            </div>
          </div>
        </div>
      </div>
    );
}
 
export default NewsDetail;
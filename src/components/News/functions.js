import { alertConfirm, alertSuccess } from '../Alert';
import { ApiService } from '../../GeneralServices';
import { useSelector, useDispatch } from 'react-redux';
import { setNews } from './newsSlice';
//Constants
import { 
    REQUEST_METHOD_GET, 
    API_URL,
    NEWS_TABLE
} from "../../constants";

// fetch the news from the db and store them into the redux store
export const getNews = (setData) => {
    ApiService({
        API_URL: API_URL,
        BASE: NEWS_TABLE,               
        METHOD: REQUEST_METHOD_GET
    })
    .then(res => {
        console.log('Success GET : ', res);
        setData(res.entries);
    })
    .catch(error => {
        console.log(error)
    })
}
// edit-updates the news received as a param
export const editNews = (news) => {
    // here would go the request to edit the news
    const successEditNews = {
        title: 'Noticia editada',
        text: 'La noticia se editó correctamente',
        confirmText: 'Aceptar'
    }
    alertSuccess(successEditNews)
}

export const deleteNews = (id) => {    
    const alertDeleteNews = {
        title: '¿Desea eliminar la Noticia?',
        text: 'Al aceptar la noticia se eliminará',
        confirmText: 'Aceptar',
        cancelText: 'Cancelar',
        // here would go the request to delete the news
        confirmFunction: () => console.log('noticia eliminada, id: ' + id) 
    }

    alertConfirm(alertDeleteNews)    
}

import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectNews, getNews, deleteNews } from '../../features/news/newsSlice';

import MaterialTable from '@material-table/core';
import { FaEdit, FaTrash } from 'react-icons/fa';
import { options, localization, title } from './settings';
import { cloneDeep } from 'lodash';
import { FaPlus } from 'react-icons/fa';

import { Button } from '@chakra-ui/button';
import moment from 'moment';

import NewsForm from '../NewsForm'
import LoaderComponent from '../loader/LoaderComponent';
import { IDLE, SUCCEEDED } from '../../constants';

const TableNews = () => {
    
    const dispatch = useDispatch();

    const [modal, setModal] = useState  ("")

    const columns = [
        { 
            title: 'Imagen', 
            field: 'image',
            render: rowData => <img src={rowData.image} alt={`${rowData.name}`} style={{width: 60, borderRadius: '60%'}}/>,
            cellStyle: { width: '10%' } 
        },
        { 
            title: 'Nombre', 
            field: 'name',
            cellStyle: { textAlign: 'center', width: '40%' }
        },    
        { 
            title: 'Fecha de creación', 
            field: 'createdAt',                        
            render: rowData => moment.utc(rowData.date).format('DD/MM/YYYY')
        },
        {                
            cellStyle: { width: '3%' },
            render: rowData => 
            <Button mr={0} colorScheme="yellow" onClick={()=>handleEditeNews(rowData)}>
                    <FaEdit />
                </Button>
        },
        {
            cellStyle: { width: '3%' },
            render: rowData => 
            <Button colorScheme="red" onClick={()=>handleDeleteNews({name:rowData.name, id:rowData.id})}>
            <FaTrash />
        </Button>            
        }
    ];

    const handleDeleteNews = (dataNews)=>{
        dispatch(deleteNews(dataNews))
        dispatch(getNews())        
    }


    const handleEditeNews = (dataNews) =>{       
        setModal(<NewsForm newsObject={dataNews} setModal={setModal}/>)
        dispatch(getNews());        
    }  
    
    const data = useSelector(selectNews);
    const newsStatus = useSelector(state => state.news.status);

    // Cloned data in order to avoid immutability and be able to work with Material UI Table
    const flexData = cloneDeep(data);

    useEffect(() => {
        if(newsStatus === IDLE)   
            dispatch(getNews());
        
    }, [modal, dispatch]);



    return (
        <div className="md:w-11/12 xl:w-100 mx-auto mb-5">
            { newsStatus !== SUCCEEDED ? 
        <div className='flex justify-center items-center' style={{'min-height': '60vh'}}>
            <LoaderComponent />
        </div>
        :
        <>
            <MaterialTable
                columns={columns}
                data={flexData}
                title={title}
                options={options}
                localization={localization}
                actions={[
            {
              icon: () => <div style={{fontSize: '1em', color: 'rgb(40,110,251)'}}><FaPlus /></div>,
              tooltip: "Crear Usuario",
              position: "toolbar",
              onClick: () => {
                setModal(<NewsForm setModal={setModal} />);
              }
            }
          ]}
            />
            {modal}
            </>
        }
        </div>
    );
}

export default TableNews;

/*
    Implemented lodash cloneDeep method to make extensible copies of the redux-store data in order to be
    displayable in a Material UI Table.
*/
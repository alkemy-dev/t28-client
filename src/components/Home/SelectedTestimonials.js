import React, { Fragment, useEffect } from 'react';
import { Text, Center, Flex } from '@chakra-ui/react';
import TestimonialCard from './TestimonialCard';
import { useDispatch, useSelector } from 'react-redux';
import { getTestimonies, selectLatestTestimonies } from '../../features/testimony/testimonySlice';



const SelectedTestimonials = () => { 
    const data = useSelector(selectLatestTestimonies);
    const dispatch = useDispatch();
    
    
    useEffect(() => {
        
            dispatch(getTestimonies());
        
    }, [dispatch]);
   
    return (
        <Fragment>
            <div className="background-yellow py-1">
                <Center>
                    <Text fontSize="4xl" my='2'>Testimonios</Text>
                </Center>            
                <Flex justify="center" flexWrap="wrap">       
                    {data.map((item, index) => {
                        return(
                            <TestimonialCard key={item.index} data={item}/>   
                        )                    
                    })}                
                </Flex>
            </div> 
        </Fragment>
    )
  }

  export default SelectedTestimonials;
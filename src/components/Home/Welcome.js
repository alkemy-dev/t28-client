import React, { useState, useEffect } from 'react';
import './Welcome.css';

const Welcome = ()  => {
    
    const [message, setMessage] = useState('');    

    useEffect(() => {
        setMessage('Sección de Bienvenida')
    }, [])

    return(
        <section className="flex items-center justify-center section-height background-yellow">
            <div className="text-center">                
                <h2 className="mt-6 text-3xl font-bold md:text-5xl">
                    {message}
                </h2>                
            </div>
        </section>
    )
}

export default Welcome;
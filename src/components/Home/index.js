import React from 'react';
import Slider from '../Slider';
import Welcome from './Welcome';
import LatestNews from './LatestNews';
import Testimonials from './SelectedTestimonials';

const index = () => {
    return(        
        <React.Fragment>
            <Slider />
            <Welcome />
            <LatestNews />
            <Testimonials />
        </React.Fragment>
    )
}

export default index;
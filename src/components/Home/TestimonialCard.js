import React, { Fragment } from 'react';
import { Link } from "react-router-dom";
import { 
    Box, 
    Image, 
    Badge, 
    Button, 
    Text
} 
from '@chakra-ui/react';

const TestimonialCard = ({ data }) => {    
        
    return(        
        <Fragment>
            <Box m={5} maxW="340px" borderWidth="1px" borderRadius="lg" overflow="hidden" className="bg-gray-50">
                <Image h="250px" src={data.image} alt={`${data.name}'s img`} />        
                <Box p="6">
                    <Box d="flex" justifyContent="space-between" mb={2}>
                            
                      <Box            
                      
                      fontWeight="semibold"
                      as="h4"
                      lineHeight="tight"
                      >
                        {data.name}
                    	</Box>   
                                 
                    </Box>            
                          
                    
                        <Text color="gray.500" isTruncated p={0}>
                            {(data.content).replace(/(<([^>]+)>)/ig, '')}
                        </Text>
                                    
                </Box>
            </Box>
        </Fragment>
    )
}

export default TestimonialCard;
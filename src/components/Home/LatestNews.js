import React, { Fragment, useEffect } from 'react';
import { Text, Center, Flex } from '@chakra-ui/react';
import NewsCard from './NewsCard';
import { useSelector, useDispatch } from 'react-redux';
import { selectLatestNews, getNews } from '../../features/news/newsSlice';
import { IDLE } from '../../constants/index';

const LatestNews = () => { 
    const data = useSelector(selectLatestNews);
    const dispatch = useDispatch();
    
    const newsStatus = useSelector(state => state.news.status);

    useEffect(() => {
        if( newsStatus === IDLE && data.length === 0) {
            dispatch(getNews());
        }
    }, [newsStatus]);
   
    return (
        <Fragment>
            <div className="background-yellow py-1">
                <Center>
                    <Text fontSize="4xl" my='2'>Últimas novedades</Text>
                </Center>            
                <Flex justify="center" flexWrap="wrap">       
                    {data.map((item) => {
                        return(
                            <NewsCard key={item.id} data={item}/>   
                        )                    
                    })}                
                </Flex>
            </div> 
        </Fragment>
    )
  }

  export default LatestNews;
import React from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Button,
  Input,
  Textarea,
  Text,
} from "@chakra-ui/react";
import {
  EMPTY_VALUE_MESSAGE,
  INITIAL_VALUES_HOME_FORM,
  MINIMUM_VALUE_MESSAGE,
} from "../../constants";

import Swal from "sweetalert2";

/*THIS COMPONENT NEEDS   isOpen, onClose FROM "useDisclosure()"" FROM CHAKRA HOOKS AS AN ATRIBUTES*/
const HeaderForm = ({ isOpen, onClose }) => {
  const validationSchema = Yup.object({
    wellcome: Yup.string()
      .min(20, MINIMUM_VALUE_MESSAGE)
      .required(EMPTY_VALUE_MESSAGE),
    title1: Yup.string().required(EMPTY_VALUE_MESSAGE),
    title2: Yup.string().required(EMPTY_VALUE_MESSAGE),
    title3: Yup.string().required(EMPTY_VALUE_MESSAGE),
  });
  const formik = useFormik({
    initialValues: INITIAL_VALUES_HOME_FORM,
    validationSchema: validationSchema,
    onSubmit: (values) => {
      const img1 = { img: values.image1, title: values.title1 };
      const img2 = { img: values.image2, title: values.title2 };
      const img3 = { img: values.image3, title: values.title3 };
      const images = [img1, img2, img3];

      if (!img1.img || !img2.img || !img3.img) {
        //validation if "img" exist

        onClose();
        Swal.fire({
          icon: "error",
          title: "Error al actualizar informacion",
          text: "No se cargaron todos las imagenes del formulario",
        });
      } else {
        //UPDATE YOUR HOME STATE
        formik.resetForm();
        onClose();
        Swal.fire({
          icon: "success",
          title: "Inicio actualizado",
          text:
            "Se actualizo correctamente el mensaje de bienvenida y las imagenes",
        });
      }
    },
  });

  const handleChange = (e) => {
    const id = e.currentTarget.id;
    formik.setFieldValue(id, e.currentTarget.files[0]);
    /*  bg="green.300"
    document.getElementById(id).setAttribute("bg", "green.300"); */
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <form onSubmit={formik.handleSubmit}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader bg="blue.300" color="whiteAlpha.800">
            CAMBIAR INFORMACION
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6} bg="blue.500" color="whitesmoke">
            <FormControl mt={4}>
              <FormLabel htmlFor="wellcome">Mensaje principal</FormLabel>
              <Textarea
                name="wellcome"
                id="wellcome"
                placeholder="Bienvenidos a nuestra ONG!"
                onChange={formik.handleChange}
              />
              {formik.touched.wellcome && formik.errors.wellcome ? (
                <Text color="tomato" bg="orange.100">
                  {formik.errors.wellcome}
                </Text>
              ) : null}
            </FormControl>
            <FormControl mt={4}>
              <Input
                id="title1"
                name="title1"
                onChange={formik.handleChange}
                placeholder="Titulo de la imagen 1"
              />
              <Input
                type="file"
                id="image1"
                name="image1"
                accept="image/*"
                onChange={handleChange}
              />

              {formik.touched.title1 && formik.errors.title1 ? (
                <FormControl>
                  <Text color="tomato" bg="orange.100">
                    {formik.errors.title1}
                  </Text>
                </FormControl>
              ) : null}
            </FormControl>
            <FormControl mt={4}>
              <Input
                id="title2"
                name="title2"
                onChange={formik.handleChange}
                placeholder="Titulo de la imagen 2"
              />
              <Input
                type="file"
                id="image2"
                name="image2"
                accept="image/*"
                onChange={handleChange}
              />
              {/* <ErrorMessage name="title2" /> */}
              {formik.touched.title2 && formik.errors.title2 ? (
                <Text color="tomato" bg="orange.100">
                  {formik.errors.title2}
                </Text>
              ) : null}
            </FormControl>
            <FormControl mt={4}>
              <Input
                id="title3"
                name="title3"
                onChange={formik.handleChange}
                placeholder="Titulo de la imagen 3"
              />
              <Input
                type="file"
                id="image3"
                name="image3"
                accept="image/*"
                onChange={handleChange}
              />
              {formik.touched.title3 && formik.errors.title3 ? (
                <Text color="tomato" bg="orange.100">
                  {formik.errors.title3}
                </Text>
              ) : null}
            </FormControl>
          </ModalBody>
          <ModalFooter bg="blue.300">
            <Button type="submit" colorScheme="red" mr={3}>
              Save
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </form>
    </Modal>
  );
};

export default HeaderForm;

import React from "react";
import { shallow } from "enzyme";
import HeaderForm from "./HeaderForm";

describe("HeaderForm", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<HeaderForm />);
    expect(wrapper).toMatchSnapshot();
  });
});

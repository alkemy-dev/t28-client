import React from "react";
import { shallow } from "enzyme";
import SocialMedia from "./SocialMedia";

describe("SocialMedia", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<SocialMedia />);
    expect(wrapper).toMatchSnapshot();
  });
});

import React from 'react';
import Header from '../Header/Header';
import Footer from '../footer/FooterComponent';


const Layout = ({children}) => {
    
    const title = "Alkemy Challenge JS";
    
    return ( 
        <>
            <Header />
            <div className="bg-red min-h-screen mt-10">
                <div className="container mx-auto">
                    
                    <main className="pt-10">
                        {children}
                    </main>

                </div>
            </div>
            
            <Footer
                title={title}
            />
        </>
     );
}
 
export default Layout;
import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import FormUpdateMyProfile from './FormUpdateMyProfile';

const MyProfile = () => {

    const classTitle = `text-3xl font-sans font-bold text-black-500 text-center my-4`;
    const classText1 = `font sans font-bold text-blue-500`;
    const classText2 = `font sans font-normal text-black`;
    const classButton = `bg-blue-400 rounded-lg	uppercase font-bold py-3 px-6 text-white cursor-pointer`;

    const [update, setUpdate] = useState(false);


    const onClickDelete = () => {
        console.log("eliminando...");
    }
    // retrive logged user from redux store
    const user = useSelector(state => state.user.user);

    return (
        <div className="md:w-3/5 xl:w-3/5 mx-auto">
            <h1 className={classTitle}>Mi Perfil</h1>
            
            <div className="grid grid-cols-2 bg-gray-100 px-5 py-5 rounded-lg mb-5">
                <div className={classText1}>Nombre: </div>
                <div className={classText2}>{user.name}</div>
                <div className={classText1}>Apellido: </div>
                <div className={classText2}>{user.surname}</div>
                <div className={classText1}>Email: </div>
                <div className={classText2}>{user.email}</div>
                <div className={classText1}>Role: </div>
                <div className={classText2}>{user.roleId}</div>
                <div className={classText1}>Organization: </div>
                <div className={classText2}>{user.organizationId}</div>
            </div>

            <div className="justify-center flex items-end px-5 py-5 rounded-lg mb-5 text-center gap-5">
                <button
                    className={classButton}
                    onClick={setUpdate}
                >Editar</button>
                <button
                    className={classButton}
                    onClick={onClickDelete}
                >Suspender</button>
            </div>

            { update ? (
                <FormUpdateMyProfile user={user} />
             ) : null }
        </div>

    );
}
 
export default MyProfile;
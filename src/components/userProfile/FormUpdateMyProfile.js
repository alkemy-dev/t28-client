import React from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';

const FormUpdateMyProfile = ({ user }) => {

    const classForm = `bg-white rounded shadow-md px-8 pt-6 pb-8 mb-20`;
    const classTitle = `text-3xl font-sans font-bold text-black-500 text-center my-4`;
    const classLabel = `block text-black text-sm font-bold mb-2`;
    const classInput = `shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`;
    const classValidation = `my-2 bg-gray-200 border-l-4 border-blue-500 text-blue-700 p-3`;
    const classButton = `bg-blue-400 hover:bg-gray-900 w-full p-2 text-white uppercase font-bold mt-3 mb-6 cursor-pointer`;

    const formik = useFormik({
        initialValues: {
          name: user.name,
          surname: user.surname,
          email: user.email 
        },
        validationSchema: yup.object({
            name: yup.string()
                .required('El nombre es obligatorio'),
            surname: yup.string()
                .required('El apellido es obligatorio'),
            email: yup.string()
                .email('El email no es válido')
                .required('El Email es Obligatorio')
        }),
        onSubmit: updatedUser => {
            console.log(updatedUser);
        }
    });

    return (
       
        <div className="md:w-4/5 xl:w-4/5 mx-auto">

            <h2 className={classTitle}>Editar Mi Perfil</h2>

            <div className="flex justify-center mt-5">
                <div className="w-full max-w-lg">
                    <form
                    className={classForm}
                    onSubmit={formik.handleSubmit}
                    >
                        <div className="mb-4">
                            <label 
                            className={classLabel}
                            htmlFor="name"
                            >Nombre</label>
                            <input
                                type="text"
                                className={classInput}
                                placeholder="Nombre"
                                id="name"
                                value={formik.values.name}
                                onChange={formik.handleChange}
                            />

                            { formik.touched.name && formik.errors.name ? (
                                <div className={classValidation}>{formik.errors.name}</div>
                            ) : null }
                        </div>

                        <div className="mb-4">
                            <label 
                            className={classLabel}
                            htmlFor="surname"
                            >Apellido</label>
                            <input
                                type="text"
                                className={classInput}
                                placeholder="Apellido"
                                id="surname"
                                value={formik.values.surname}
                                onChange={formik.handleChange}
                            />

                            { formik.touched.surname && formik.errors.surname ? (
                                <div className={classValidation}>{formik.errors.surname}</div>
                            ) : null }
                        </div>

                        <div className="mb-4">
                            <label 
                            className={classLabel}
                            htmlFor="email"
                            >Email</label>
                            <input
                                type="text"
                                className={classInput}
                                placeholder="Email"
                                id="email"
                                value={formik.values.email}
                                onChange={formik.handleChange}
                            />

                            { formik.touched.email && formik.errors.email ? (
                                <div className={classValidation}>{formik.errors.email}</div>
                            ) : null }
                        </div>

                        <input 
                            type="submit"
                            className={classButton}
                            value="Editar Post"
                        />

                    </form>
                </div>
            </div>
        </div>
    );
}
 
export default FormUpdateMyProfile;
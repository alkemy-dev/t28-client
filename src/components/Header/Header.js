import React, { useState } from "react";
import { Flex, Button, IconButton } from "@chakra-ui/react";
import { Link, Redirect, Route, useHistory } from "react-router-dom";
import { GiHamburgerMenu } from "react-icons/gi";
import { AiOutlineClose } from "react-icons/ai";
import {
  HOME_ROUTE,
  ABOUT_US_ROUTE,
  USER_ROUTE,
  NEWS_ROUTE,
  ACTIVITIES_ROUTE,
  CONTACTS_ROUTE,
  TESTOMINIES_ROUTE,
  REGISTER_ROUTE,
} from "../../constants/index";
import "./Header.css";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../features/user/userSlice";

const Header = () => {
  const logo = "/logo.png";
  const { user } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const history = useHistory();
  const [display, changeDisplay] = useState("none");
  const linksMockUp = [
    { text: "Inicio", url: HOME_ROUTE },
    { text: "Nosotros", url: ABOUT_US_ROUTE },
    { text: "Actividades", url: ACTIVITIES_ROUTE },
    { text: "Novedades", url: NEWS_ROUTE },
    { text: "Testimonios", url: TESTOMINIES_ROUTE },
    { text: "Contacto", url: CONTACTS_ROUTE },
    { text: "Contribuye", url: "#" },
  ];

  return (
    <div>
      <div align="center" className="contentHeader">
        {/* Desktop */}
        <Flex display={["none", "none", "none", "flex"]} className="navHeader">
          <Flex justifyContent="flex-start">
            <Link key="Inicio" to="/">
              <img
                className="w-20 mr-6"
                src={logo}
                title="ONG-LOGO"
                alt="ONG-LOGO"
              />
            </Link>
          </Flex>
          <Flex justifyContent="flex-end">
            {linksMockUp.map((link) => (
              <Link key={link.text} to={link.url}>
                <Button
                  as="a"
                  variant="ghost"
                  aria-label="About"
                  my={5}
                  w="80%"
                >
                  {link.text}
                </Button>
              </Link>
            ))}

            {/* REGISTER AND LOGIN */}
            {!user.firstName || !user ? (
              <>
                <Link to={REGISTER_ROUTE}>
                  <Button
                    as="a"
                    variant="ghost"
                    aria-label="About"
                    my={5}
                    w="80%"
                  >
                    Registro
                  </Button>
                </Link>
                <Button
                  as="a"
                  variant="ghost"
                  aria-label="About"
                  my={5}
                  w="80%"
                  onClick={() => {
                    history.push("/login");
                  }}
                >
                  Login
                </Button>
              </>
            ) : (
              <>
                <Link to={"/backoffice"}>
                  <Button
                    as="a"
                    variant="ghost"
                    aria-label="About"
                    my={3}
                    bg="lightblue"
                    w="80%"
                  >
                    {user.firstName}
                  </Button>
                </Link>
                <Button
                  as="a"
                  variant="ghost"
                  aria-label="About"
                  my={3}
                  w="70%"
                  bg="yellow.300"
                  cursor="pointer"
                  onClick={() => {
                    dispatch(logout());
                  }}
                >
                  LOGOUT
                </Button>
              </>
            )}
            {/* REGISTER AND LOGIN END */}
          </Flex>
        </Flex>

        {/* Mobile */}
        <Flex justify="flex-end">
          <IconButton
            aria-label="Open Menu"
            size="lg"
            mr={2}
            icon={<GiHamburgerMenu color="red" />}
            onClick={() => changeDisplay("flex")}
            display={["flex", "flex", "flex", "none"]}
          />
        </Flex>
      </div>

      {/* Mobile Content */}
      <Flex
        w="100vw"
        display={display}
        bgColor="gray.50"
        zIndex={20}
        h="100vh"
        pos="fixed"
        top="0"
        left="0"
        overflowY="auto"
        flexDir="column"
      >
        <Flex justify="flex-end">
          <IconButton
            mt={2}
            mr={2}
            aria-label="Open Menu"
            size="lg"
            icon={<AiOutlineClose />}
            onClick={() => changeDisplay("none")}
          />
        </Flex>

        <Flex flexDir="column" align="center">
          {linksMockUp.map((link) => (
            <Link key={link.text} to={link.url} my={4} mx={12}>
              <Button as="a" variant="ghost" aria-label="About" my={2} w="100%">
                {link.text}
              </Button>
            </Link>
          ))}

          {!user.firstName && !user.roleId ? (
            <>
              <Link to={REGISTER_ROUTE} my={4} mx={12}>
                <Button
                  as="a"
                  variant="ghost"
                  aria-label="About"
                  my={0}
                  w="100%"
                >
                  Registro
                </Button>
              </Link>
              <Button
                as="a"
                variant="ghost"
                aria-label="About"
                my={0}
                w="100%"
                onClick={() => {
                  history.push("/login");
                }}
              >
                Login
              </Button>
            </>
          ) : (
            <>
              <Link to={"/backoffice"} my={4} mx={12}>
                <Button
                  as="a"
                  variant="ghost"
                  aria-label="About"
                  my={1}
                  bg="lightblue"
                  w="100%"
                >
                  {user.firstName}
                </Button>
              </Link>
              <Button
                as="a"
                variant="ghost"
                aria-label="About"
                my={1}
                w="100%"
                bg="yellow.300"
                onClick={() => {
                  dispatch(logout());
                }}
              >
                LOGOUT
              </Button>
            </>
          )}
        </Flex>
      </Flex>
    </div>
  );
};

export default Header;

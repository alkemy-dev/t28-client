import React from "react";

const Slide = ({ data }) => {
  return (
    <div className="slide">
      <div className="text">{data.text}</div>
      <div
        className="image"
        style={{ backgroundImage: `url(${data.imageUrl})` }}
      ></div>
    </div>
  );
};

export default Slide;

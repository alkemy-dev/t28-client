import React from "react";
import Slider from "react-slick";

//Styles
import "./Slider.sass";

//Utils
import { settings } from "./settings";

//Components
import Slide from "./Slide";

//Images
import imageOne from './img/Foto1.jpg';
import imageThree from './img/Foto2.jpg';
import imageFive from './img/Foto3.jpg';
import imageFour from './img/Foto4.jpg';

//fake Data
const fakeData = [
  {
    id: '1',
    imageUrl: imageOne
  },
  {
    id: '2',
    imageUrl: imageThree
  },
  {
    id: '3',
    imageUrl: imageFive
  },
  {
    id: '4',
    imageUrl: imageFour
  },
];

const index = () => {
  return (
    <div className="slider">
      <Slider {...settings}>
        {fakeData.map((slide) => (
          <Slide key={slide.id} data={slide} />
        ))}
      </Slider>
    </div>
  );
};

export default index;

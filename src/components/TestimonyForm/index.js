import React, { useState } from "react";
import { Formik } from "formik";
import { Box, ButtonGroup, Modal, ModalBody, ModalCloseButton, ModalContent, ModalHeader, ModalOverlay, useDisclosure } from "@chakra-ui/react";
import { InputControl, SubmitButton } from "formik-chakra-ui";

//Utils
import { validationSchema, emptyValues } from "./utils";

//Functions
import { onSubmit } from "./functions";

//Constants
import {
  EMPTY_TESTIMONY_ERROR_TITLE,
  EMPTY_TESTIMONY_ERROR_TEXT,
  EMPTY_TESTIMONY_ERROR_CONFIRM_TEXT,
} from "../../constants";

//Components
import CKEditor from "./CKEditor";
import { alertError } from "../Alert";
import { useDispatch } from "react-redux";
import { getTestimonies } from "../../features/testimony/testimonySlice";

const TestimonyForm = ({ testimony, setModal }) => {

  const dispatch = useDispatch()
  const { onOpen } = useDisclosure();
  const [mode, setMode] = useState(true)
  const type = testimony ? "Editar" : "Crear";

  const onClose = ()=>{
    setMode(false)
    setModal("")
}


  const initialValues = testimony ? testimony : emptyValues;
  const [content, setContent] = useState(testimony ? testimony.content : "");

  const handleSubmitFormik = (e) => {
    setMode(false)
    setModal("")
    dispatch(getTestimonies())
    if (content !== "") {
      //Create Form Data Object
      const formData = e;
      formData.content = content;

      //Determines if creates or edits
      testimony ? onSubmit({ testimony, formData }) : onSubmit({ formData });
      return true;
    } else {
      //If Content if empty throught error
      alertError({
        title: EMPTY_TESTIMONY_ERROR_TITLE,
        text: EMPTY_TESTIMONY_ERROR_TEXT,
        confirmText: EMPTY_TESTIMONY_ERROR_CONFIRM_TEXT,
      });
      return false;
    }    
  };

  
  return (
    
     <Modal size="xl" isOpen={mode} onClose={onClose} onOpen={onOpen}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{type} Entrada</ModalHeader>
          <ModalCloseButton />
          <ModalBody>  
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmitFormik}
        validationSchema={validationSchema}
      >
        {({ handleSubmit, values, errors }) => (
          <Box
            borderWidth="1px"
            rounded="lg"
            shadow="1px 1px 3px rgba(0,0,0,0.3)"
            p={6}
            m="50px auto"
            as="form"
            onSubmit={handleSubmit}
            width="100%"
          >
            <InputControl m="25px auto" name="name" label="Titulo" />
            <InputControl m="25px auto" name="image" label="Imagen" />
            <CKEditor content={content} setContent={setContent} />
            <ButtonGroup m="25px auto">
              <SubmitButton size="lg">
                {testimony ? "Editar Testimonio" : "Crear Testimonio"}
              </SubmitButton>
            </ButtonGroup>

            {/* <Box as="pre" marginY={10}>
              {JSON.stringify(values, null, 2)}
              <br />
              {JSON.stringify(errors, null, 2)}
            </Box> */}
          </Box>
        )}
      </Formik>
      </ModalBody>
        </ModalContent>
      </Modal>
    
  );
};

export default TestimonyForm;

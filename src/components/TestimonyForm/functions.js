import { ApiService } from "../../GeneralServices";
import { alertError, alertSuccess } from "../Alert";

import {  
  REQUEST_METHOD_POST,
  API_URL,
} from "../../constants";

import { errorCreate, errorEdit } from "./utils";

export const onSubmit = ({ testimony, formData }) => {
  console.log("Hello from onSubmit /functions.js");
  console.log(formData, testimony);
  
  //Request Options
  const options = {
    API_URL: API_URL,
    METHOD: REQUEST_METHOD_POST,
    data: formData,
    BASE: "testimonials",
  };

  // Modify Options if Editing
  if (testimony) {
    options.METHOD = 'PUT';
    options.ID = testimony.id;
  }

  ApiService(options)
    .then((res) => {      
        //Handle Alert Error (Edit/Create)
        const mode = testimony ? "edito" : "creo"
        const successAlert = {
          title: "Exito!",
          text: `El testimonio ${formData.name} se ${mode} correctamente`,
          confirmText: "Aceptar",
        };
        alertSuccess(successAlert);
    })
    .catch((err) => {
      //Handle Alert Error (Edit/Create)
      if (testimony) {
        alertError(errorEdit);
      } else {
        alertError(errorCreate);
      }
      console.log(err);
    });
};

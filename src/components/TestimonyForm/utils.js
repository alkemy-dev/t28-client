import * as Yup from "yup";
import {
  CREATE_TESTIMONY_ERROR_TITLE,
  CREATE_TESTIMONY_ERROR_TEXT,
  CREATE_TESTIMONY_ERROR_CONFIRM_TEXT,
  EDIT_TESTIMONY_ERROR_TITLE,
  EDIT_TESTIMONY_ERROR_TEXT,
  EDIT_TESTIMONY_ERROR_CONFIRM_TEXT,
} from "../../constants";

//Form Validation Schema
export const validationSchema = Yup.object({
  name: Yup.string().required("Ingrese un titulo"),
  image: Yup.string().required("Ingrese una imagen"),
});

//Form Empty Values
export const emptyValues = {
  name: "",
  image: "",
};

export const errorCreate = {
  title: CREATE_TESTIMONY_ERROR_TITLE,
  text: CREATE_TESTIMONY_ERROR_TEXT,
  confirmText: CREATE_TESTIMONY_ERROR_CONFIRM_TEXT,
};

export const errorEdit = {
  title: EDIT_TESTIMONY_ERROR_TITLE,
  text: EDIT_TESTIMONY_ERROR_TEXT,
  confirmText: EDIT_TESTIMONY_ERROR_CONFIRM_TEXT,
};

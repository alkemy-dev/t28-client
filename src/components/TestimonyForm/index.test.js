import React from "react";
import { shallow } from "enzyme";
import TestimonyForm from "./index";

describe("HeaderForm", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<TestimonyForm />);
    expect(wrapper).toMatchSnapshot();
  });
});

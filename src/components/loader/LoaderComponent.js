import React from "react";

import Loader from "react-loader-spinner";

/* type styles: Audio
                Ball-Triangle
                Bars
                Circles
                Grid
                Hearts
                Oval
                Puff
                Rings
                ThreeDots 

    ONG Colors: #DB5752
                #FAFA88
                #9AC9FB

    Set Timeout in milliseconds...

    Example: <LoaderComponent type = {"TailSpin"} color = {"#DB5752"} height = {80} width = {80} />
*/

const LoaderComponent = ({type = "TailSpin", color = "#DB5752", height = 80, width = 80, timeout }) => {
    
    return <Loader type={type} color={color} height={height} width={width} timeout={timeout}/>           
         
};

export default LoaderComponent;

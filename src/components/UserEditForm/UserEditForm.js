import React, { useState /*  useEffect  */ } from "react";
import * as Yup from "yup";
import { Field, useFormik } from "formik";
import {
  Radio,
  RadioGroup,
  Stack,
  FormControl,
  FormLabel,
  Input,
  Select,
  Box,
  Container,
  ButtonGroup,
  Button,
  Text,
  Heading,
} from "@chakra-ui/react";
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalOverlay,
} from "@chakra-ui/modal";
import { useDisclosure } from "@chakra-ui/hooks";
import { useSelector, useDispatch } from 'react-redux';


import { handleActionUser } from './functions';
import { EMPTY_VALUE_MESSAGE, REQUEST_METHOD_POST, REQUEST_METHOD_PUT } from "../../constants";

const UserEditForm = ({ userObj, setModal }) => {
  const loggedUser = useSelector(state => state.user.user);
  const dispatch = useDispatch();
  const { onOpen } = useDisclosure();
  const [mode, setMode] = useState(true);

  const [initialValues] = useState(
    userObj ? userObj : { firstName: "", lastName:"", email: "", password: "", image: "", organizationId: "", roleId: "" }
  );
  const type = userObj ? "Editar" : "Crear";
  const action = userObj ? REQUEST_METHOD_PUT : REQUEST_METHOD_POST;

  const onSubmit = async (data) => {
    handleActionUser(action, data, dispatch);
    setMode(false)
    setModal("")
  };

  const onClose = ()=>{
      setMode(false)
      setModal("")
  }

  const validationSchema = Yup.object({
    firstName: Yup.string().required(EMPTY_VALUE_MESSAGE),
    lastName: Yup.string().required(EMPTY_VALUE_MESSAGE),
    password: Yup.string().required(EMPTY_VALUE_MESSAGE),
    email: Yup.string().required(EMPTY_VALUE_MESSAGE)
  });

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: validationSchema,
    onSubmit: onSubmit
  });

  return (
    <Modal isOpen={mode} onClose={onClose} onOpen={onOpen}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{type} Usuario</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
    
        <FormControl borderRadius="2xl" color="GrayText">
          
          <form onSubmit={formik.handleSubmit} id="user-form">
            <FormLabel htmlFor="firstName" mt="5">
              Nombre
            </FormLabel>
            <Input
              type="text"
              name="firstName"
              id="name"
              defaultValue={initialValues.firstName}
              onChange={formik.handleChange}
            />
            {formik.touched.firstName && formik.errors.firstName ? (
              <Text color="tomato" bg="orange.50">
                {formik.errors.firstName}
              </Text>
            ) : null}
            
            <FormLabel htmlFor="lastName" mt="5">
              {" "}
              Apellido
            </FormLabel>
            <Input
              type="text"
              name="lastName"
              id="lastName"
              defaultValue={initialValues.lastName}
              onChange={formik.handleChange}
            />
            {formik.touched.lastName && formik.errors.lastName ? (
              <Text color="tomato" bg="orange.50">
                {formik.errors.lastName}
              </Text>
            ) : null}

            <FormLabel htmlFor="email" mt="5">
              {" "}
              Email
            </FormLabel>
            <Input
              type="email"
              name="email"
              id="email"
              defaultValue={initialValues.email}
              onChange={formik.handleChange}
            />
            {formik.touched.email && formik.errors.email ? (
              <Text color="tomato" bg="orange.50">
                {formik.errors.email}
              </Text>
            ) : null}
          {/* Password Field only available to Admin users trying to create a new User */}
            { loggedUser.roleId.toString() === '1' && action === 'POST' ?
              (<>
                <FormLabel htmlFor="password" mt="5">
                {" "}
                Password
              </FormLabel>
              <Input
                type="password"
                name="password"
                id="password"
                defaultValue={'' || initialValues.password}
                onChange={formik.handleChange}
              />
              {formik.touched.password && formik.errors.password ? (
                <Text color="tomato" bg="orange.50">
                  {formik.errors.password}
                </Text>
              ) : null}
              </>)
            : <></>
            }
            
            
            <FormLabel htmlFor="organizationId" mt="5">
              {" "}
              Organization Id
            </FormLabel>
          
            <Select
              name="organizationId"
              id="organizationId"
              defaultValue={initialValues.organizationId.toString() || '1'}
              onChange={formik.handleChange}
            >
              <option value="1">Somos Más</option>
              <option value="2">ONG - 2</option>
              <option value="3">ONG - 3</option>
            </Select>
            {formik.touched.organizationId && formik.errors.organizationId ? (
              <Text color="tomato" bg="orange.50">
                {formik.errors.organizationId}
              </Text>
            ) : null}
          {/* Values available for change only if logged user is administrator */}
            {loggedUser.roleId.toString() === "1" ? (
              <RadioGroup defaultValue={initialValues.roleId.toString() || '2'} mt="5">
                <Stack direction="row">
                  <Radio
                    value="2"
                    name="roleId"
                    id="user"
                    onChange={formik.handleChange}
                  >
                    Usuario
                  </Radio>

                  <Radio
                    value="1"
                    name="roleId"
                    id="admin"
                    onChange={formik.handleChange}
                  >
                    Administrador
                  </Radio>
                </Stack>
              </RadioGroup>
            ) : (
              <></>
            )}

            
          </form>
        </FormControl>
    
    </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} type="submit" form="user-form">
              Save
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
  );
};

export default UserEditForm;
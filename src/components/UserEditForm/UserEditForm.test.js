import React from "react";
import { shallow } from "enzyme";
import UserEditForm from "./UserEditForm";

describe("UserEditForm", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<UserEditForm />);
    expect(wrapper).toMatchSnapshot();
  });
});

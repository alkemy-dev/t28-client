import { ApiService } from "../../GeneralServices";
import { alertError, alertSuccess } from "../Alert";
import {API_URL, AUTH_TABLE, AUTH_REGISTER, USERS_TABLE, REQUEST_METHOD_POST, ACCEPT, ERROR } from "../../constants";
import { addUser, updateUser } from '../../features/user/userSlice';

export const handleActionUser = (action, data, dispatch) => {
		// organizationId 1 === Somos Mas
    // roleId 2 === user
    const dataQuery = {
        "id": data.id,
        "firstName": data.firstName,
        "lastName": data.lastName,
        "email": data.email,
        "organizationId": data.organizationId || 1,
        "roleId": data.roleId || 2,
      }
      if(action === REQUEST_METHOD_POST) dataQuery.password = data.password;
      console.log('DATA QUERY : ', dataQuery);
    ApiService({
      API_URL: API_URL,
      BASE: action === REQUEST_METHOD_POST ? `${USERS_TABLE}/${AUTH_TABLE}/${AUTH_REGISTER}` : `${USERS_TABLE}/${data.id}`,
      METHOD: action,
      data: dataQuery  
    })
      .then((res) => {
        const mode = action === REQUEST_METHOD_POST ? "creo" : "edito";
        if (res === undefined)
          throw new Error("Error al editar o crear el usuario " + data.email);
        const successAlert = {
          title: "Usuario",
          text: `El usuario ${data.email} se ${mode} correctamente`,
          confirmText: ACCEPT,
        };
        alertSuccess(successAlert);
        if(action === REQUEST_METHOD_POST) {
        	dispatch(addUser(res.data));
        } else {
        	dispatch(updateUser(res.data));
        }
      })
      .catch((error) => {
        console.log(error);
        const errorAlert = {
          title: ERROR,
          text: error.message,
          confirmText: ACCEPT,
        };
        alertError(errorAlert);
      });
}